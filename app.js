﻿var cluster = require('cluster');

// Code to run if we're in the master process
if (cluster.isMaster) {

    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    console.log(cpuCount);

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Listen for dying workers
    cluster.on('exit', function (worker) {
        // Replace the dead worker,
        // we're not sentimental
        console.log('Worker ' + worker.id + ' died :(');
        cluster.fork();

    });

} else {

    //require('newrelic');

    var express = require('express');

    var app = module.exports = express();

    var views = require('./views/views');
    var views_weixin = require('./views/weixin');
    var views_services = require('./views/services');
    var views_products = require('./views/products');

    var ejs = require('ejs');

    var httpProxy = require('http-proxy');
    var proxy = new httpProxy.createProxyServer();

    var CONFIG = require('./settings/settings');

    var logger = require('./utils/logger.js');

    //var RedisStore = require('connect-redis')(express);

    var MongoStore = require('connect-mongo')(express);

    var wechat = require('wechat');

    /*
     * Middlewares
     */
    var isWeixin = function (req, res, next) {
        var ua = req.headers['user-agent'];
        if (ua && ua.match(/MicroMessenger/i) == "MicroMessenger") {
            req.body.is_weixin = 1;
            req.query.is_weixin = 1;
            next();
        } else {
            req.body.is_weixin = null;
            req.query.is_weixin = null;
            next();
        }
    };

    var initLocalVars = function (req, res, next) {
        var weixinInfo = {
            is_weixin: req.query.is_weixin || req.body.is_weixin || 0
        };
        var langCode = req.cookies['lang-code'] || 'zh';
        res.locals.LANG_CODE = langCode;
        res.locals.STATIC_URL = CONFIG.STATIC_URL;
        res.locals.SERVER_DATA = '{}';
        res.locals.STATIC_CSS = [];
        res.locals.STATIC_JS = ['views/simple-view.js'];
        res.locals.BUILD = CONFIG.BUILD;
        res.locals.weixin_info = JSON.stringify(weixinInfo);
        next();
    };

    //cross domain need mid-ware
    var allowCrossDomain = function (req, res, next) {
        var origin = (req.headers.origin || "*");
        if (req.method.toUpperCase() === 'OPTIONS') {
            res.header('Access-Control-Allow-Credentials', true);
            res.header('Access-Control-Allow-Origin', origin);
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'X-Requested-With');
            return ( res.end() );
        }
        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Origin', origin);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'X-Requested-With');
        next();
    };

    // 错误处理中间件
    var errorHandler = function (err, req, res, next) {
        var isXhr = req.xhr;
        var errorMsg = "program fatal error " + err.toString() + err.stack;
        //请求是ajax请求，不进行页面跳转
        if (isXhr) {
            res.json({
                "error_code": "40001",
                "error": errorMsg,
                "request": ""
            });
            logger.error(req.originalUrl, req.ip, errorMsg);
            return;
        } else {
            res.render('error/error.html', {
                SERVER_DATA: {},
                STATIC_URL: CONFIG.STATIC_URL,
                STATIC_CSS: [],
                STATIC_JS: [],
                weixin_info: {},
                BUILD: CONFIG.BUILD
            });
            logger.error(req.originalUrl, req.ip, errorMsg);
        }
    };

    var apiMeiweiProxy = function (req, res, next) {
        req.headers.host = CONFIG.API_HOSTNAME + ':' + CONFIG.API_PORT;
        proxy.web(req, res, {
            target: CONFIG.API_URL
        });
    };

    console.log('......................................');

    /*
     * Configurations
     * NODE_ENV=production node app.js, 正式运行后修改
     */
    app.configure(function () {

        app.set('views', __dirname + '/templates');

        app.set('view engine', 'html');

        app.engine('html', ejs.renderFile);

        if (CONFIG.PROXY_ON) {
            app.use('/api/meiwei/', apiMeiweiProxy);
        }

        app.use(express.cookieParser('my secret here'));

        app.use(express.bodyParser());

        //    app.use(express.session({
//        secret: "keyboard cat",
//        store: new RedisStore({
//            host:CONFIG.REDIS_SERVER,
//            port:CONFIG.REDIS_PORT
////            max_attempts:10000
//        })
//    }));

        app.use('/api/weixin', express.session({
            secret: "keyboard cat",
            store: new MongoStore({
                db: CONFIG.MONGO_DB,
                host: CONFIG.MONGO_SERVER,
                port: CONFIG.MONGO_PORT,
                username: CONFIG.MONGO_USER,
                password: CONFIG.MONGO_PASSWORD,
                auto_reconnect: true
            })
        }));

        app.use('/api/weixin', wechat(CONFIG.WEIXIN_TOKEN, views_weixin.handleWxRequest));

        app.use(express.methodOverride());

        //app.use(allowCrossDomain);

        app.use(isWeixin);

        app.use(initLocalVars);

        app.use(app.router);

        app.use(errorHandler);

        app.use('/assets', express.static(__dirname + '/assets', {
            maxAge: 86400000
        }));

    });

    /*
     * Routes
     */
    //app.get('/', views.home);
    //app.get('/', views.apppromo);
    app.get('/', views.newHome);
    app.get('/newhome', views.newHome);

    app.get('/restaurant/search', views.restaurantSearch);
    app.get('/restaurant/:restaurant_id', views.restaurantDetail);
    app.get('/restaurant/:restaurant_id/order', views.restaurantOrder);
    app.get('/restaurant/:restaurant_id/pictures', views.restaurantPictures);

    app.get('/member/login', views.memberLogin);
    app.get('/member/register', views.memberRegister);
    app.get('/member', views.memberDashboard);

    app.get('/staff', views.staffDashboard);
    app.get('/admin/staff',views.staffAdminDashboard);

    app.get('/ad/apppromo', views.apppromo);
    app.get('/ad/gewara', views.gewara);

    app.get('/aboutus/:static_name', views.staticPages);

    app.get('/product/cake', views_products.cake);
    app.get('/product', views_products.productList);
    app.get('/product/:product_id', views_products.productDetail);

    app.get('/services/airport', views_services.orderAirport);
    app.get('/services/pingan', views_services.orderPingAn);
    app.get('/vvip/dashboard', views_services.vvipDashboard);

    app.get('/staff/coupon_validate', views_services.couponValidate);

    app.get('/weixin/login', views_weixin.wxlogin);
    app.post('/weixin/binding', allowCrossDomain, views_weixin.wxBinding);
    app.get('/weixin/menu', views_weixin.menu);
    app.get('/weixin/auth2_redirect', views_weixin.wxCallbackAuth);
    app.post('/weixin/sendservicemsg', allowCrossDomain, views_weixin.wxServiceMessage);

    /*
     * Launch App
     */
    app.listen(CONFIG.LISTEN_PORT, CONFIG.BINDING_IP);

    console.log("Express server listening on port " + CONFIG.LISTEN_PORT);
    logger.info('server start listen port:' + CONFIG.LISTEN_PORT);
    console.log('......................................');

}
