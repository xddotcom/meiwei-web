module.exports = function(grunt) {
    var nodemonIgnoredFiles = [
        'README.md',
        'Gruntfile.js',
        '/assets/',
        '/src/',
        '/.git/',
        '/node_modules/',
        '/.sass-cache/'
    ];
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: { },
            mwapp: {
                src: [
                    'src/js/app/_open.js',
                    'src/js/app/app.js',
                    'src/js/app/app-local.js',
                    'src/js/app/i18n.js',
                    'src/js/app/templates.js',
                    'src/js/app/utils.js',
                    'src/js/app/model.js',
                    'src/js/app/models/*.js',
                    'src/js/app/view.js',
                    'src/js/app/router.js',
                    'src/js/app/_close.js',
                ],
                dest: 'assets/js/mw-app.js'
            },
            bootstrap: {
                src: [
                    'src/js/bootstrap/transition.js',
                    'src/js/bootstrap/alert.js',
                    'src/js/bootstrap/button.js',
                    'src/js/bootstrap/carousel.js',
                    'src/js/bootstrap/collapse.js',
                    'src/js/bootstrap/dropdown.js',
                    'src/js/bootstrap/modal.js',
                    'src/js/bootstrap/tooltip.js',
                    'src/js/bootstrap/popover.js',
                    'src/js/bootstrap/scrollspy.js',
                    'src/js/bootstrap/tab.js',
                    'src/js/bootstrap/affix.js'
                ],
                dest: 'assets/js/bootstrap.js'
            }
        },
        uglify: {
            mwapp: {
                options: {
                    mangle: false,
                    beautify: { width: 80, beautify: true }
                },
                files: [
                    {
                        src: 'assets/js/mw-app.js',
                        dest: 'assets/js/mw-app-min.js'
                    }, {
                        expand: true,
                        src: '*.js',
                        dest: 'assets/js/views/',
                        cwd: 'src/js/app/views/'
                    }
                ]
            },
            plugins: {
                files: {
                    'assets/js/plugins.js': [
                        'src/js/plugins/unslider.js',
                        'src/js/plugins/moment.js',
                        'src/js/plugins/moment.zh-cn.js',
                        'src/js/plugins/datetimepicker.js',
                        'src/js/plugins/datetimepicker.zh-CN.js'
                     ]
                }
            }
        },
        less: {
            options: {
                cleancss: true,
                report: 'min'
            },
            bootstrap: {
                src: 'src/less/bootstrap.less',
                dest: 'assets/css/bootstrap.css'
            },
            bootstrap_theme: {
                src: 'src/less/bootstrap-theme.less',
                dest: 'assets/css/bootstrap-theme.css'
            },
            mwapp: {
                files: [
                    {
                        src: ['src/less/main.less'],
                        dest: 'assets/css/main.css'
                    }, {
                        expand: true,
                        src: '*.less',
                        dest: 'assets/css/views/',
                        cwd: 'src/less/views/',
                        ext: '.css'
                    }
                ]
            }
        },
        templates: {
            all: {
                files: {
                    'assets/js/templates.js': ['src/template/**/*.html']
                }
            }
        },
        watch: {
            scripts_bootstrap: {
                files: ['src/bootstrap/**/*.js'],
                tasks: ['concat:bootstrap']
            },
            scripts_mwapp: {
                files: ['src/js/app/**/*.js'],
                tasks: ['concat:mwapp', 'uglify:mwapp']
            },
            scripts_plugin: {
                files: ['src/js/plugins/**/*.js'],
                tasks: ['uglify:plugins']
            },
            less: {
                files: ['src/less/**/*.less'],
                tasks: ['less']
            },
            templates: {
                files: ['src/template/**/*.html'],
                tasks: ['templates']
            }
        },
        nodemon: {
            dev: {
                options: {
                    file: 'app.js',
                    args: ['development'],
                    watchedExtensions: [
                        'js',
                        // This might cause an issue starting the server
                        // See: https://github.com/appleYaks/grunt-express-workflow/issues/2
                        // 'coffee'
                    ],
                    // nodemon watches the current directory recursively by default
                    // watchedFolders: ['.'],
                    debug: true,
                    delayTime: 1,
                    ignoredFiles: nodemonIgnoredFiles,
                }
            },
        },
        concurrent: {
            dist: {
                tasks: ['concat', 'less', 'templates', 'uglify'],
                options: { logConcurrentOutput: true }
            },
            server: {
                tasks: ['watch', 'nodemon:dev'],
                options: { logConcurrentOutput: true }
            }
        }
    });

    grunt.loadTasks('tasks');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-nodemon');
    
    // Configurable port number
    var port = grunt.option('port');
    if (port) grunt.config('connect.server.options.port', port);
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.registerTask('server', 'concurrent:server');
    grunt.registerTask('dist', 'concurrent:dist');

};
