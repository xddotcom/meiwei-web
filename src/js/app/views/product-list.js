$(function () {
    var initialList = [];
    MeiweiApp.Router.route('cake', function () { initialList = [39, 40, 41]; });
    MeiweiApp.Router.route('bcake', function () { initialList = [39]; });
    MeiweiApp.Router.route('ccake', function () { initialList = [40]; });
    MeiweiApp.Router.route('dessert', function () { initialList = [41]; });
    MeiweiApp.Router.route('flower', function () { initialList = [21]; });
    MeiweiApp.Router.route('style', function () { initialList = [32]; });
    MeiweiApp.Router.route('wine', function () { initialList = [31]; });
    MeiweiApp.Router.route('misc', function () { initialList = [28]; });
    MeiweiApp.Router.route('car', function () { initialList = [29]; });
    MeiweiApp.Router.route('yacht', function () { initialList = [34]; });
    MeiweiApp.Router.route('pjet', function () { initialList = [30]; });
    Backbone.history.start();

    var ProductCategoryList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['product-category-box'],
            className: 'product-category-box',
            events: {
                "click .product-nav a": "filterProductItemList"
            },
            initModelView: function () {
                var description = this.model.get('description');
                if (description) {
                    this.model.set('parent_description', description);
                    this.model.unset('description');
                }
            },
            filterProductItemList: function (e) {
                var $el = $(e.currentTarget);
                var itemsIds = $el.data('filter').toString().split(','); //List of digit strings!
                this.$('.product-nav a').removeClass('active');
                $el.addClass('active');
                this.$('.product-item').addClass('hidden');
                for (var i = 0, l = itemsIds.length; i < l; i++) {
                    this.$('.product-item[data-item=product-item' + itemsIds[i] + ']').removeClass('hidden');
                }
            },
            render: function () {
                MeiweiApp.ModelView.prototype.render.call(this);
                this.$('.product-nav a').first().trigger('click');
                return this;
            }
        })
    });

    MeiweiApp.Pages.ProductListPage = new (MeiweiApp.PageView.extend({
        events: {
            "click .category-nav a": "filterProductList"
        },
        initPage: function () {
            this.products = new MeiweiApp.Collections.Products();
            this.productsFromServer = new MeiweiApp.Collections.Products();
            this.products.comparator = 'comparator_id';
            this.views = {
                productCategoryList: new ProductCategoryList({
                    el: this.$('.product-box-list'),
                    collection: this.products
                })
            };
        },
        filterProductList: function (e) {
            var $el = $(e.currentTarget);
            var categoryIds = $el.data('filter').toString().split(','); //List of digit strings!
            this.$('.category-nav a').removeClass('active');
            $el.addClass('active');
            $el.closest('.subnav').siblings('a').addClass('active');
            if (categoryIds.length == 1) {
                var categoryId = categoryIds[0];
                this.products.reset(categoryId == 0 ?
                    this.productsFromServer.toJSON() :
                    this.productsFromServer.where({id: +categoryId}));
            } else if (categoryIds.length > 1) {
                this.products.reset(this.productsFromServer.filter(function (product) {
                    return _.contains(categoryIds, product.id.toString());
                }));
            }
        },
        initSubFilter: function (collection) {
            collection.get(39).set('filter', [
                {
                    name: '美位推荐', items: "118,124,121,117"
                },
                {
                    name: '宝宝', items: "116,117,118,159"
                },
                {
                    name: '女士', items: "122,123,124,161"
                },
                {
                    name: '绅士', items: "119,120,121,160"
                }
            ]);
            collection.get(41).set('filter', [
                {
                    name: '美位推荐', items: "131,130,133,126"
                },
                {
                    name: 'Cupcake', items: "126,127,128"
                },
                {
                    name: '饼干曲奇', items: "129"
                },
                {
                    name: '马卡龙', items: "130,131,132"
                },
                {
                    name: '巧克力', items: "133,134,135,136"
                }
            ]);
            collection.get(21).set('filter', [
                {
                    name: '美位推荐', items: "146,150,156,147"
                },
                {
                    name: '鲜花花束', items: "142,146,147,148,174"
                },
                {
                    name: '鲜花花盒', items: "150,151,152,153"
                },
                {
                    name: '永生花盒', items: "154,155,156,157"
                },
                {
                    name: '花牌', items: "158"
                }
            ]);
        },
        render: function () {
            var self = this;
            this.productsFromServer.fetch({
                data: {category: 1},
                success: function (collection, response, options) {
                    self.initSubFilter(collection);
                    var initialCollection = _.isEmpty(initialList) ? collection.toJSON() :
                        collection.filter(function (product) {
                            return _.contains(initialList, product.id);
                        });
                    self.products.reset(initialCollection);
                }
            });
        }
    }))({el: $("#view-product-list")});

    MeiweiApp.Pages.ProductListPage.go(serverData);
});
