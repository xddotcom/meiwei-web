$(function () {
    var ContactList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: Mustache.compile('{{name}} {{mobile}}'),
            tagName: 'option',
            render: function () {
                MeiweiApp.ModelView.prototype.render.call(this);
                this.$el.attr({
                    'data-name': this.model.get('name'),
                    'data-mobile': this.model.get('mobile'),
                    'data-gender': this.model.get('sexe'),
                    'value': this.model.get('id')
                });
                return this;
            }
        }),
        addAll: function () {
            MeiweiApp.CollectionView.prototype.addAll.call(this);
            this.$el.prepend('<option data-gender="0">或者选择一个联系人</option>');
        }
    });

    var GuestLists = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: Mustache.compile('<span>{{name}}{{genderText}}, {{id_no}}</span>' +
                '<input type="hidden" name="guest[name]" value={{name}}>' +
                '<input type="hidden" name="guest[gender]" value={{gender}}>' +
                '<input type="hidden" name="guest[id_no]" value={{id_no}}>' +
                '&nbsp&nbsp<a class="btn btn-default btn-xs">删除</a>'),
            tagName: 'p',
            className: 'text-right',
            events: { 'click .btn': 'removeGuest' },
            removeGuest: function () {
                this.model.destroy();
            }
        })
    });

    var OrderAirportForm = MeiweiApp.View.extend({
        events: {
            'submit': 'orderAirportSubmit',
            'click .add-client': 'addClient',
            'change input[name=service_type]': 'switchOrderType',
            'change input[name=datetime]': 'onChangeTime',
            'change .contact-list': 'setContactInfo'
        },
        initView: function () {
            _.bindAll(this, 'orderAirportSubmit', 'onSuccess', 'onError');
            this.guests = new MeiweiApp.Collection();
            this.contacts = new MeiweiApp.Collections.Contacts();
            this.views = {
                contactList: new ContactList({ collection: this.contacts, el: this.$('.contact-list') }),
                guestList: new GuestLists({ collection: this.guests, el: this.$('.guest-list') })
            };
            this.listenTo(MeiweiApp.me, 'login', function(){
                this.contacts.fetch();
            });
        },
        addClient: function () {
            var name = this.$('input[name=guest_name]').val() || null;
            var id_no = this.$('input[name=guest_id_no]').val() || null;
            var gender = this.$('select[name=guest_gender]').val() || 0;
            var genderText = this.$('select[name=gender] option:selected').text() || null;
            if (name && id_no && genderText) {
                this.guests.add(new MeiweiApp.Model({
                    name: name, id_no: id_no, gender: gender, genderText: genderText
                }));
            }
            this.$('input[name=guest_name],input[name=guest_id_no]').val('');
        },
        setContactInfo: function (e) {
            var el = $(e.target).find('option:selected');
            this.$('input[name=name]').val(el.data('name'));
            this.$('select[name=sexe]').val(el.data('sexe'));
            this.$('input[name=mobile]').val(el.data('mobile'));
        },
        onChangeTime: function () {
            var datetime = this.$('input[name=datetime]').val();
            if (new Date(datetime) - new Date() < 1000 * 60 * 60 * 24) {
                this.$('select[name=accompany]').val(0).attr('disabled', 'disabled');
            } else {
                this.$('select[name=accompany]').removeAttr('disabled');
            }
        },
        switchOrderType: function () {
            var value = this.$('input[name=service_type]:checked').val();
            this.$('.datetime-addon').html(value == 'departure' ?
                MeiweiApp._('Airport Departure Time') :
                MeiweiApp._('Airport Arrival Time'));
            this.$('.airport-addon').html(value == 'departure' ?
                MeiweiApp._('Airport Departure') :
                MeiweiApp._('Airport Arrival'));
        },
        onSuccess: function (model, response, options) {
            var orderId = model.get('id');
            var order = new MeiweiApp.Models.GenericOrder({id:orderId});
            order.fetch({
                success:function(model){
                    window.location.href = model.get('payment').payment_url;
                },
                error:function(){

                }
            })
        },
        onError: function (model, xhr, options) {
            this.displayError(this.$el, xhr.responseText);
        },
        orderAirportSubmit: function (e) {
            if (e.preventDefault) e.preventDefault();
            var self = this;
            var fields = {}, inputFields = ['name','mobile', 'flight_no', 'car_no',
                'service_type', 'datetime', 'airport',
                'accompany', 'address', 'comment']
            for (var i = 0; i < inputFields.length; i++) {
                var name = inputFields[i];
                fields[name] = this.$('[name=' + name + ']').val() || null;
            }
            fields['service_type'] = this.$('input[name=service_type]:checked').val();
            fields['gender'] = this.$('select[name=gender]').val();

            if (this.guests.length == 0) this.addClient();
            fields.guests = this.guests.toJSON();
            this.order = new MeiweiApp.Models.AirportOrderCreation(fields);
            var orderJson = this.order.toJSON();
            orderJson.amount = this.calculateAmount();
            if(this.order.get('guests').length){
                orderJson.name_local = this.order.get('guests')[0].name;
            }
            var serviceType = this.order.get('service_type');
            if (serviceType == 'arrival') {
                orderJson.arrival = true;
            } else {
                orderJson.arrival = false;
            }
            orderJson.datetime = moment(orderJson.datetime).format('LLL');
            var orderHtml = TPL["service-vvip-orders-preview"](orderJson);
            MeiweiApp.showConfirmDialog(orderHtml, {
                onConfirm: function () {
                    self.order.save({}, { success: self.onSuccess, error: self.onError });
                },
                onCancel: function () {

                },
                title:'<h4 data-i18n="Please confirm your order">请确认您的订单</h4>'
            });
            $('#confirm-dialog').find('.btn-confirm').text('确认并支付');
            $('#confirm-dialog').find('.btn-cancel').text('返回并修改');
        },
        renderTimeinput: function () {
            var now = new Date(), tomorrow = new Date();
            tomorrow.setDate(now.getDate() + 3);
            tomorrow.setMinutes(0);
            if (MeiweiApp.isMobile) {
                var str = tomorrow.toISOString();
                this.$('input[name=datetime]').attr('value', str.slice(0, str.lastIndexOf('.')));
                this.$('input[name=datetime]').attr('type', 'datetime-local');
            } else {
                this.$('input[name=datetime]').datetimepicker({
                    todayBtn: false,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'zh-CN',
                    minuteStep: 1,
                    showMeridian: true,
                    format: "yyyy-mm-dd hh:ii"
                }).datetimepicker('setStartDate', now).datetimepicker('update', tomorrow);
            }
        },
        calculateAmount: function () {
            var amount = 800;
            var airportArray = ['PVG', 'SHA', 'PEK', 'CAN'];
            if (_.contains(airportArray,this.order.get('airport'))) {
                amount = 1500;
            }
            var delta = Math.round((new Date(this.order.get('datetime')) - new Date()) / 1000);
            if (delta < 3 * 60 * 60) {
                amount *= 2
            }
            var guests = this.order.get('guests').length;
            var accompany = this.order.get('accompany') || 0;
            amount = amount * guests + accompany * 500;
            return amount
        },
        render: function () {
            this.renderTimeinput();
            this.$('.order-type .btn').trigger('click');
            this.contacts.fetch();
        }
    });

    MeiweiApp.Pages.OrderAirport = new (MeiweiApp.PageView.extend({
        initPage: function () {
            this.views = {
                orderAirportForm: new OrderAirportForm({ el: this.$('.order-form') })
            };
        },
        render: function () {
            this.views.orderAirportForm.render();
        }
    }))({el: $("#view-order-airport")});

    MeiweiApp.Pages.OrderAirport.go({status: serverData.status});
});
