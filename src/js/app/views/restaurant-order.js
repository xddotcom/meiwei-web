$(function () {
    var ContactList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: Mustache.compile('{{name}} {{mobile}}'),
            tagName: 'option',
            render: function () {
                MeiweiApp.ModelView.prototype.render.call(this);
                this.$el.attr({
                    'data-name': this.model.get('name'),
                    'data-mobile': this.model.get('mobile'),
                    'data-sexe': this.model.get('sexe'),
                    'value': this.model.get('id')
                });
                return this;
            }
        }),
        addAll: function () {
            MeiweiApp.CollectionView.prototype.addAll.call(this);
            this.$el.prepend('<option>或者选择一个联系人</option>');
        }
    });

    MeiweiApp.Pages.RestaurantOrder = new (MeiweiApp.PageView.extend({
        events: {
            'change .contact-list': 'setContactInfo',
            'click #order-submit-confirm': 'confirmOrder',
            'submit #restaurant-order-form': 'submitOrder'
        },
        initPage: function () {
            _.bindAll(this, 'renderHourList');
            this.restaurant = new MeiweiApp.Models.Restaurant();
            this.contacts = new MeiweiApp.Collections.Contacts();
            this.order = new MeiweiApp.Models.Order();
            this.hours = new MeiweiApp.Models.Hour();
            this.views = {
                contactList: new ContactList({ collection: this.contacts, el: this.$('.contact-list') })
            };
            this.listenTo(this.restaurant, 'change', this.setRestaurantInfo);
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
        },
        setRestaurantInfo: function (model) {
            this.hours.fetch({
                url: this.restaurant.get('hours'),
                success: this.renderHourList
            });
            this.$('.restaurant-info').html(TPL['restaurant-info'](model.toJSON()));
            if(this.restaurant.get('restaurant_type')==20){
                this.$('#defaultSeatType').text('卡座');
            } else{
                this.$('#defaultSeatType').text('大厅');
            }
        },
        setOrderInfo: function () {
            this.$('input[name=orderdate]').val(this.order.get('orderdate'));
            var personNum = this.order.get('personnum') || 1;
            this.$('input[name=personnum]').val(personNum);
        },
        setContactInfo: function (e) {
            var el = $(e.target).find('option:selected');
            this.$('input[name=name]').val(el.data('name'));
            this.$('select[name=sexe]').val(el.data('sexe'));
            this.$('input[name=mobile]').val(el.data('mobile'));
        },
        renderHourList: function () {
            var ymd = this.$('input[name=orderdate]').val().split('-');
            var day = (ymd.length == 3 ? (new Date(ymd[0], ymd[1] - 1, ymd[2])).getDay().toString() : '0');
            var $select = this.$('select[name=ordertime]');
            $select.empty();
            var hour = this.hours.get(day);
            if (hour && hour.length > 0) {
                for (var i = 0; i < hour.length; i++) {
                    var item = hour[i];
                    var $option = $('<option></option>').val(item[0]).html(item[0] + ' ' + item[1]);
                    if (this.order.get('ordertime') == item[0]) {
                        $option = $('<option selected></option>').val(item[0]).html(item[0] + ' ' + item[1]);
                    }
                    $select.append($option);
                }
            }
        },
        submitOrder: function (e) {
            var self = this;
            this.newOrder = new MeiweiApp.Models.Order({
                restaurant: this.restaurant.get('id'),
                contactname: this.$('input[name=name]').val(),
                contactgender: +this.$('select[name=sexe]').val(),
                contactphone: this.$('input[name=mobile]').val(),
                orderdate: this.$('input[name=orderdate]').val(),
                ordertime: this.$('select[name=ordertime]').val(),
                personnum: this.$('input[name=personnum]').val(),
                clubseattype: this.$('input[name=clubseattype]:checked').val(),
                other: this.$('textarea[name=other]').val()
            });
            var attrs = this.newOrder.toJSON();
            attrs.restaurant_fullname = this.restaurant.get('fullname');
            var orderHtml = TPL["restaurant-order-success"](attrs);
            MeiweiApp.showConfirmDialog(orderHtml, {
                onConfirm: function () {
                    self.newOrder.save({}, {
                        success: function (model, xhr, options) {
                            if (model.get('payment_order')) {
                                window.location.href = '/member/';
                            } else {
                                window.location.href = '/member/';
                            }
                        },
                        error: function (model, xhr, options) {
                            self.displayError(self.$('#restaurant-order-form'), xhr.responseText);
                        }
                    });
                },
                onCancel: function () {

                },
                title: '<h4 data-i18n="Please confirm your order">请确认您的订单</h4>'
            });
        },
        renderTimeinput: function () {
            var self = this;
            if (MeiweiApp.isMobile) {
                var str = (new Date()).toISOString();
                this.$('input[name=orderdate]').attr('value', self.order.get('orderdate') || str.substring(0, 10));
                this.$('input[name=orderdate]').attr('type', 'date');
            } else {
                this.$('input[name=orderdate]').datetimepicker({
                    todayBtn: false,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'zh-CN',
                    minView: 'month',
                    format: "yyyy-mm-dd"
                }).datetimepicker('setStartDate', new Date())
                    .datetimepicker('update', self.order.get('orderdate') || new Date());
            }
        },
        render: function () {
            this.renderTimeinput();
            this.contacts.fetch();
            if (this.options.order) {
                this.order.set(this.options.order);
                this.setOrderInfo();
            }
            if (this.options.restaurant) {
                this.restaurant.set(this.options.restaurant);
            } else if (this.options.restaurantId) {
                this.restaurant.clear({silent: true});
                this.restaurant.set({id: this.options.restaurantId}, {silent: true});
                this.restaurant.fetch();
            }
        }
    }))({el: $("#view-restaurant-order")});

    MeiweiApp.Pages.RestaurantOrder.go(serverData);
});
