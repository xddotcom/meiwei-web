$(function() {
    var RestaurantListItem = MeiweiApp.ModelView.extend({
        className: 'restaurant-list-item',
        template: TPL['restaurant-list-item'],
        events: { 'click > header': 'viewRestaurant' },
        viewRestaurant: function() {
            window.location.href = '/restaurant/' + this.model.id;
        }
    });
    
    var RestaurantList = MeiweiApp.CollectionView.extend({
        ModelView: RestaurantListItem
    });
    
    var FilterModelView = MeiweiApp.ModelView.extend({
        tagName: 'span',
        className: 'filter-item',
        render: function() {
            this.$el.html($('<a href="#"></a>').html(this.model.get('name')));
            this.$el.attr('data-filter-value', this.model.id);
            return this;
        }
    });
    var CuisineFilter = MeiweiApp.CollectionView.extend({
        ModelView: FilterModelView.extend({ attributes: { 'data-filter-key': 'cuisine' } })
    });
    var CircleFilter = MeiweiApp.CollectionView.extend({
        ModelView: FilterModelView.extend({ attributes: { 'data-filter-key': 'circle' } })
    });
    var RecommendFilter = MeiweiApp.CollectionView.extend({
        ModelView: FilterModelView.extend({ attributes: { 'data-filter-key': 'recommend' } })
    });
    
    MeiweiApp.Pages.RestaurantSearch = new (MeiweiApp.PageView.extend({
        events: {
            'click .filter-item': 'filterRestaurant',
            'shown.bs.collapse .collapse': 'pinFilterPanel'
        },
        initPage: function() {
            this.restaurants = new MeiweiApp.Collections.Restaurants();
            this.cuisines = new MeiweiApp.Collections.Cuisines();
            this.circles = new MeiweiApp.Collections.Circles();
            this.recommends = new MeiweiApp.Collections.RecommendNames();
            this.views = {
                restaurantList: new RestaurantList({ collection: this.restaurants, el: this.$('.result-list') }),
                cuisineFilter: new CuisineFilter({ collection: this.cuisines, el: this.$('#collapse-cuisines .panel-body') }),
                circleFilter: new CircleFilter({ collection: this.circles, el: this.$('#collapse-circles .panel-body') }),
                recommendFilter: new RecommendFilter({ collection: this.recommends, el: this.$('#collapse-recommends .panel-body') })
            };
            this.initPageNav(this, this.restaurants);
            $(window).scroll(this.pinFilterPanel);
        },
        pinFilterPanel: function () {
            var gap = Math.max(0, this.$('.results').outerHeight() - this.$('.filter-panel').outerHeight());
            var scrollTop = $(window).scrollTop();
            var top = Math.min(scrollTop, gap);
            if (scrollTop > gap) {
                var offset = gap - scrollTop;
                this.$('.filter-panel').css('top',  offset + 'px');
            } else {
                this.$('.filter-panel').css('top',  0);
            }
        },
        fetchRestaurants: function(filter) {
            this.$('.loading-result').removeClass('hidden');
            this.listenToOnce(this.restaurants, 'reset', function() {
                this.$('.loading-result').addClass('hidden');
            });
            if (filter) {
                this.restaurants.fetch({ reset: true, data: filter });
            } else {
                this.restaurants.fetch({ reset: true });
            }
        },
        filterRestaurant: function(e) {
            if (e.preventDefault) e.preventDefault();
            var item = e.currentTarget;
            var filter = {};
            filter[$(item).attr('data-filter-key')] = $(item).attr('data-filter-value');
            this.fetchRestaurants(filter);
            window.scrollTo(0, 0);
        },
        render: function() {
            this.fetchRestaurants(this.options.filter);
            this.cuisines.fetch({ reset: true });
            this.circles.fetch({ reset: true });
            this.recommends.fetch({ reset: true });
        }
    }))({el: $("#view-restaurant-search")});
    
    MeiweiApp.Pages.RestaurantSearch.go({filter: serverData});
});
