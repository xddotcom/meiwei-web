$(function() {
    var RegisterStreamItem = MeiweiApp.ModelView.extend({
        className: 'register-stream-item',
        template: TPL['register-stream-item'],
    });
    
    var RegisterStream = MeiweiApp.CollectionView.extend({
        ModelView: RegisterStreamItem
    });

    var recommendItems = [
        {
            title:"赵小姐不等位(长乐路店)",
            image:"http://meiwei.u.qiniudn.com/media/restaurant/84ff60608cb164a4f9fc5ea74a1c8e74b7451da7.jpg",
            link:"/restaurant/263"
        },
        {
            title:"香港农圃荟",
            image:"http://meiwei.u.qiniudn.com/media/restaurant/d129b5c03d5be8df0fae743ee294b768ea353a8a.jpg",
            link:"/restaurant/257"
        },
        {
            title:"东方私宴",
            image:"http://meiwei.u.qiniudn.com/media/restaurant/21754d64cfde840d1697dcce4a3355cc3edffc8a.jpg",
            link:"/restaurant/258"
        },
        {
            title:"翠园",
            image:"http://meiwei.u.qiniudn.com/media/restaurant/93a2e613ed648f1b7b7ebb2336815cbb47342efd.jpg",
            link:"/restaurant/259"
        },
        {
            title:"薰衣草花盒",
            image:"http://meiwei.u.qiniudn.com/media/product/6395eafcb2688dc2da74940a335dbde13497798c.jpg",
            link:"/product/155"
        },
        {
            title:"Pretty Girl 蛋糕",
            image:"http://meiwei.u.qiniudn.com/media/product/cd81d77a7e167ce3d728a4b69c9b20bf82f207c5.jpg",
            link:"/product/117"
        },
        {
            title:"超人 Superman",
            image:"http://meiwei.u.qiniudn.com/media/product/e3a2a060e13363f94ec2d0ff6987722acf88da6d.jpg",
            link:"/product/118"
        },
        {
            title:"芳心可可",
            image:"http://meiwei.u.qiniudn.com/media/product/9330ce833a98b8afc901d31bb8ae69f71c1c2f9d.jpg",
            link:"/product/127"
        }
    ];

    var MemberSigninForm = MeiweiApp.View.extend({
        events: { 'submit': 'signin' },
        signin: function(e) {
            if (e.preventDefault) e.preventDefault();
            var username = this.$('input[name=username]').val();
            var password = this.$('input[name=password]').val();
            var self = this;
            if (username.length > 0 && password.length > 0) {
                MeiweiApp.me.login({ username : username, password : password }, {
                    success: function() {
                        location.href = "/member/";
                    },
                    error: function(model, xhr, options) {
                        self.displayError(self.$el, xhr.responseText);
                    }
                });
            } else {
            	this.$('.non_field_errors').html('请输入用户名和密码！');
            }
        }
    });
    
    var MemberSignupForm = MeiweiApp.View.extend({
        events: { 'submit': 'signup' },
        login: function(username, password) {
            MeiweiApp.me.login({ username : username, password : password }, {
                success: function() {
                    var redirect = localStorage.getItem('register-redirect');
                    localStorage.removeItem('register-redirect');
                    location.href = redirect || "/member/";
                },
            });
        },
        signup: function(e) {
            if (e.preventDefault) e.preventDefault();
            var username = this.$('input[name=username]').val() || null;
            var password = this.$('input[name=password]').val() || null;
            var passwordConfirm = this.$('input[name=password-confirm]').val() || null;
            if (password != passwordConfirm) {
                this.$('.non_field_errors').html(MeiweiApp._('两次密码输入不一致，请重新输入。'));
            } else if (username && password) {
                var onLoginSuccess = this.onLoginSuccess;
                var onLoginFail = this.onLoginFail;
                var self = this;
                MeiweiApp.me.register({username: username, password: password}, {
                    success: function() {
                        self.login(username, password);
                    },
                    error: function(model, xhr, options) {
                        self.displayError(self.$el, xhr.responseText);
                    }
                });
            } else {
                this.$('.non_field_errors').html('请输入用户名和密码！');
            }
        }
    });
    
    MeiweiApp.Pages.MemberLogin = new (MeiweiApp.PageView.extend({
        events: {
            'show.bs.tab .toggle-sign-in a': 'toggleSignIn',
            'show.bs.tab .toggle-sign-up a': 'toggleSignUp'
        },
        initPage: function() {
            this.recommend = new MeiweiApp.Collection();
            this.views = {
                stream: new RegisterStream({
                    collection: this.recommend,
                    el: this.$('.stream')
                }),
                signinForm: new MemberSigninForm({ el: this.$('#sign-in-form') }),
                signupForm: new MemberSignupForm({ el: this.$('#sign-up-form') })
            };
        },
        toggleSignIn: function() {
            this.$('.toggle-sign-in').addClass('hide');
            this.$('.toggle-sign-up').removeClass('hide');
        },
        toggleSignUp: function() {
            this.$('.toggle-sign-up').addClass('hide');
            this.$('.toggle-sign-in').removeClass('hide');
        },
        render: function() {
            this.recommend.reset(recommendItems);
            if (this.options.status == 'signin') {
                this.$('.toggle-sign-in a').tab('show');
            } else {
                this.$('.toggle-sign-up a').tab('show');
            }
        }
    }))({el: $("#view-member-login")});
    
    MeiweiApp.Pages.MemberLogin.go({status: serverData.status});
    
});
