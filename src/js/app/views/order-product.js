$(function () {
    var ContactList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: Mustache.compile('{{name}} {{mobile}}'),
            tagName: 'option',
            render: function () {
                MeiweiApp.ModelView.prototype.render.call(this);
                this.$el.attr({
                    'data-name': this.model.get('name'),
                    'data-mobile': this.model.get('mobile'),
                    'data-gender': this.model.get('sexe'),
                    'value': this.model.get('id')
                });
                return this;
            }
        }),
        addAll: function () {
            MeiweiApp.CollectionView.prototype.addAll.call(this);
            this.$el.prepend('<option data-gender="0">或者选择一个联系人</option>');
        }
    });

    var ProductDetail = MeiweiApp.ModelView.extend({
        template: TPL['product-info']
    });

    MeiweiApp.Pages.ProductDetailPage = new (MeiweiApp.PageView.extend({
        events: {
            'submit #product-order-form': 'submitOrder',
            'change .contact-list': 'setContactInfo'
        },
        initPage: function () {
            _.bindAll(this, 'setContactInfo', 'submitOrder');
            this.product = new MeiweiApp.Models.ProductItem();
            this.contacts = new MeiweiApp.Collections.Contacts();
            this.views = {
                contactList: new ContactList({
                    collection: this.contacts,
                    el: this.$('.contact-list')
                }),
                productInfo: new ProductDetail({
                    model: this.product,
                    el: this.$('.product-info')
                })
            };
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
        },
        setContactInfo: function (e) {
            var el = $(e.target).find('option:selected');
            this.$('input[name=name]').val(el.data('name'));
            this.$('select[name=gender]').val(el.data('gender'));
            this.$('input[name=mobile]').val(el.data('mobile'));
        },
        submitOrder: function (e) {
            var self = this;
            var newOrder = new MeiweiApp.Models.ProductOrderCreation();
            newOrder.set('product_id', this.product.get('id'));
            newOrder.set('datetime', this.$('input[name=datetime]').val());
            newOrder.set('name', this.$('input[name=name]').val());
            newOrder.set('gender', +this.$('select[name=gender]').val());
            newOrder.set('mobile', this.$('input[name=mobile]').val());
            newOrder.set('address', this.$('input[name=address]').val());
            newOrder.set('comment', this.$('textarea[name=comment]').val());

            var order = newOrder.toJSON();
            order.product_name = this.product.get('name');
            var orderHtml = TPL['product-order-success'](order);
            MeiweiApp.showConfirmDialog(orderHtml, {
                onConfirm: function () {
                    newOrder.save({}, {
                        success: function () {
                            window.location.href = '/member/#product';
                        },
                        error: function (model, xhr, options) {
                            self.displayError(self.$('form'), xhr.responseText);
                        }
                    });
                },
                onCancel: function () {

                },
                title:'<h4 data-i18n="Please confirm your order">请确认您的订单</h4>'
            });
        },
        renderTimeinput: function () {
            var now = new Date(), tomorrow = new Date();
            tomorrow.setDate(now.getDate() + 5);
            tomorrow.setMinutes(0);
            if (MeiweiApp.isMobile) {
                var str = tomorrow.toISOString();
                this.$('input[name=datetime]').attr('value', str.substring(0, 15));
                this.$('input[name=datetime]').attr('type', 'datetime-local');
            } else {
                this.$('input[name=datetime]').datetimepicker({
                    todayBtn: false,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'zh-CN',
                    minView: 'hour',
                    minuteStep: 30,
                    showMeridian: true,
                    format: "yyyy-mm-dd hh:ii"
                }).datetimepicker('setStartDate', new Date())
                    .datetimepicker('update', tomorrow);
            }
        },
        render: function () {
            this.renderTimeinput();
            this.contacts.fetch({success: function () {
                this.$('.contact-list').trigger('change');
            }});
            if (this.options.product_id) {
                this.product.clear();
                this.product.set({
                    id: this.options.product_id
                });
                this.product.fetch({success: function (model) {
                    //model.set('picture','http://web.clubmeiwei.com/assets/img/cake/weddin.jpg')
                }});
            }
        }

    }))({el: $("#view-order-product")});
    var options = {
        product_id: serverData.product_id
    };
    MeiweiApp.Pages.ProductDetailPage.go(options);
});
