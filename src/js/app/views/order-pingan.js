$(function () {

    var OrderPingAnSuccess = MeiweiApp.ModelView.extend({
        template: TPL['services-pingan-success'],
        initModelView: function () {
            this.listenTo(this.model, 'success', this.showOrderSuccessInfo);
        },
        showOrderSuccessInfo: function () {
            var datetime = this.model.get('datetime');
            var datetimeLocal = datetime.replace('T', ' ').substring(0, 16);
            this.model.set('datetime_local', datetimeLocal);
            var options = {
                show: true
            };
            this.$el.modal(options);
            this.model.clear({silent: true});
        },
        render: function () {
            MeiweiApp.ModelView.prototype.render.call(this);
        }
    });

    MeiweiApp.Pages.OrderPingan = new (MeiweiApp.PageView.extend({
        events: {
            'submit .pingan-form': 'orderPinganSubmit'
        },
        initPage: function () {
            _.bindAll(this, 'orderPinganSubmit', 'onSuccess', 'onError', 'render','initCouponInfo');
            this.order = new MeiweiApp.Models.PingAnOrderCreation();
            this.views = {
                orderPingAnSuccess: new OrderPingAnSuccess({
                    model: this.order,
                    el: this.$('.order-success-modal')
                })
            };
        },
        initCouponInfo: function () {
            var coupon = new (MeiweiApp.Model.extend({
                urlRoot: MeiweiApp.configs.APIHost + '/orders/coupon/',
                idAttribute: 'coupon_no'
            }))({
                'coupon_no': this.options.serverData.coupon
            });
            this.listenTo(coupon, 'change', function () {
                this.$('.menu-description').html(coupon.get('detail').description);
            });
            coupon.fetch({
                error: function () {
                    window.location.href = '/';
                }
            });
            this.$('input[name=coupon]').attr('value',this.order.get('coupon'));
        },
        onSuccess: function (model) {
            this.order.trigger('success');
        },
        onError: function (model, xhr, options) {
            this.displayError(this.$('form'), xhr.responseText);
        },
        orderPinganSubmit: function (e) {
            if (e.preventDefault) e.preventDefault();
            var self = this;
            var fields = {}, inputFields = ['name', 'gender', 'mobile', 'datetime', 'people', 'coupon', 'address']
            for (var i = 0; i < inputFields.length; i++) {
                var name = inputFields[i];
                fields[name] = this.$('[name=' + name + ']').val() || null;
            }
            this.order.set(fields);
            var orderHtml = TPL["services-pingan-order-preview"](this.order.toJSON());
            MeiweiApp.showConfirmDialog(orderHtml, {
                onConfirm: function () {
                    self.order.save({}, { success: self.onSuccess, error: self.onError });
                },
                onCancel: function () {

                },
                title:'<h4 data-i18n="Please confirm your order">请确认您的订单</h4>'
            });
//            this.order.save({}, { success: this.onSuccess, error: this.onError });
        },
        renderTimeinput: function () {
            var now = new Date(), tomorrow = new Date();
            tomorrow.setDate(now.getDate() + 5);
            if (MeiweiApp.isMobile) {
                var str = tomorrow.toISOString();
                this.$('input[name=datetime]').attr('value', str.substring(0, 15));
                this.$('input[name=datetime]').attr('type', 'date');
            } else {
                this.$('input[name=datetime]').datetimepicker({
                    todayBtn: false,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'zh-CN',
                    minView: 'hour',
                    minuteStep: 30,
                    showMeridian: true,
                    format: "yyyy-mm-dd hh:ii"
                }).datetimepicker('setStartDate', new Date())
                    .datetimepicker('update', tomorrow);
            }
        },
        render: function () {
            this.order.set(this.options.serverData);
            this.initCouponInfo();
            this.renderTimeinput();
        }
    }))({el: $("#view-order-pingan")});

    MeiweiApp.Pages.OrderPingan.go({serverData: serverData});
});
