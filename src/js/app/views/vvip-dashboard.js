$(function () {
    currentTab = '#nav-order-pending';
    MeiweiApp.Router.route('pending', function() {currentTab = '#nav-order-pending';});
    MeiweiApp.Router.route('confirmed', function() {currentTab = '#nav-order-confirmed';});
    MeiweiApp.Router.route('fulfilled', function() {currentTab = '#nav-order-fulfilled';});
    Backbone.history.start();

    var PendingOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['service-vvip-orders'],
            className: "thumbnail order-list-item",
            events: {
                "click .order-confirm": "confirmOrder"
            },
            confirmOrder: function (e) {
                var self = this;
                this.model.confirm({
                    success: function (model) {
                        MeiweiApp.displayAlert(MeiweiApp._('Successfully Confirmed Order'));
                        self.model.collection.remove(self.model);
                    },
                    error: function (model, xhr) {
                        MeiweiApp.displayAlert(xhr.responseText);
                    }
                });
            },
            initModelView: function () {
                var timeCreated = moment(this.model.get('time_created')).format('LLL');
                this.model.set('time_created', timeCreated);
                var name = this.model.get('name');
                var index = name.indexOf(',');
                if (index > 0) {
                    this.model.set('name_local', name.substring(0, index));
                } else {
                    this.model.set('name_local', name);
                }
                var serviceType = this.model.get('attributes').service_type;
                if (serviceType == 'arrival') {
                    this.model.set('arrival', true);
                } else {
                    this.model.set('arrival', false);
                }
                this.model.get('attributes').datetime = moment(this.model.get('attributes').datetime).format('LLL');
                var guests = JSON.parse(this.model.get('attributes').guests);
                this.model.get('attributes').guests = guests;

                var paymentStatus = this.model.get('payment').status;
                if (paymentStatus == 0) {
                    this.model.set('payment_status', false);
                } else {
                    this.model.set('payment_status', true);
                }

                var status = this.model.get('status');
                if (status == 0) {
                    this.model.set('order_new', true);
                } else if (status == 10) {
                    this.model.set('order_confirmed', true);
                } else if (status == 20) {
                    this.model.set('order_payable', true);
                } else if (status == 50) {
                    this.model.set('order_finished', true);
                } else if (status == 99) {
                    this.model.set('order_cancelled', true);
                }
            }
        })
    });

    var ConfirmedOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['service-vvip-orders'],
            className: "thumbnail order-list-item",
            events: {
                "click .order-finish": "finishOrder"
            },
            finishOrder: function (e) {
                var self = this;
                this.model.complete({
                    success: function () {
                        self.model.collection.remove(self.model);
                        MeiweiApp.displayAlert(MeiweiApp._('Successfully Completed Order'));
                    },
                    error: function (model, xhr) {
                        MeiweiApp.displayAlert(xhr.responseText);
                    }
                });
            },
            initModelView: function () {
                var timeCreated = moment(this.model.get('time_created')).format('LLL');
                this.model.set('time_created', timeCreated);
                var name = this.model.get('name');
                var index = name.indexOf(',');
                if (index > 0) {
                    this.model.set('name_local', name.substring(0, index));
                } else {
                    this.model.set('name_local', name);
                }
                var serviceType = this.model.get('attributes').service_type;
                if (serviceType == 'arrival') {
                    this.model.set('arrival', true);
                } else {
                    this.model.set('arrival', false);
                }
                this.model.get('attributes').datetime = moment(this.model.get('attributes').datetime).format('LLL');
                var guests = JSON.parse(this.model.get('attributes').guests);
                this.model.get('attributes').guests = guests;

                var paymentStatus = this.model.get('payment').status;
                if (paymentStatus == 0) {
                    this.model.set('payment_status', false);
                } else {
                    this.model.set('payment_status', true);
                }

                var status = this.model.get('status');
                if (status == 0) {
                    this.model.set('order_new', true);
                } else if (status == 10) {
                    this.model.set('order_confirmed', true);
                } else if (status == 20) {
                    this.model.set('order_payable', true);
                } else if (status == 50) {
                    this.model.set('order_finished', true);
                } else if (status == 99) {
                    this.model.set('order_cancelled', true);
                }
            }
        })
    });

    var FulfilledOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['service-vvip-fulfilled-orders'],
            className: "thumbnail fulfilled-order-list-item",
            initModelView: function () {
                var timeCreated = moment(this.model.get('time_created')).format('LLL');
                this.model.set('time_created', timeCreated);
                var name = this.model.get('name');
                var index = name.indexOf(',');
                if (index > 0) {
                    this.model.set('name_local', name.substring(0, index));
                } else {
                    this.model.set('name_local', name);
                }
                var serviceType = this.model.get('attributes').service_type;
                if (serviceType == 'arrival') {
                    this.model.set('arrival', true);
                } else {
                    this.model.set('arrival', false);
                }
                this.model.get('attributes').datetime = moment(this.model.get('attributes').datetime).format('LLL');
                var guests = JSON.parse(this.model.get('attributes').guests);
                this.model.get('attributes').guests = guests;

                var paymentStatus = this.model.get('payment').status;
                if (paymentStatus == 0) {
                    this.model.set('payment_status', false);
                } else {
                    this.model.set('payment_status', true);
                }

                var status = this.model.get('status');
                if (status == 0) {
                    this.model.set('order_new', true);
                } else if (status == 10) {
                    this.model.set('order_confirmed', true);
                } else if (status == 20) {
                    this.model.set('order_payable', true);
                } else if (status == 50) {
                    this.model.set('order_finished', true);
                } else if (status == 99) {
                    this.model.set('order_cancelled', true);
                }
            }
        })

    });

    MeiweiApp.Pages.VvipCenter = new (MeiweiApp.PageView.extend({
        events: {
            'show.bs.tab #nav-order-pending': 'getPendingOrders',
            'show.bs.tab #nav-order-confirmed': 'getConfirmedOrders',
            'show.bs.tab #nav-order-fulfilled': 'getFulfilledOrders'
        },
        initPage: function () {
            this.pendingOrders = new MeiweiApp.Collections.StaffAirportOrders();
            this.confirmedOrders = new MeiweiApp.Collections.StaffAirportOrders();
            this.fulfilledOrders = new MeiweiApp.Collections.StaffAirportOrders();
            this.views = {
                pendingOrderList: new PendingOrderList({
                    collection: this.pendingOrders,
                    el: this.$('#tab-order-pending')
                }),
                confirmedOrderList: new ConfirmedOrderList({
                    collection: this.confirmedOrders,
                    el: this.$("#tab-order-confirmed")
                }),
                fulfilledOrderList: new FulfilledOrderList({
                    collection: this.fulfilledOrders,
                    el: this.$("#tab-order-fulfilled")
                })
            };
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
            this.initPageNav(this, this.pendingOrders);
        },
        getPendingOrders: function () {
            this.updatePageNav(this.pendingOrders);
            this.pendingOrders.fetch({data: { status: 0 },reset:true });
        },
        getConfirmedOrders: function () {
            this.updatePageNav(this.confirmedOrders);
            this.confirmedOrders.fetch({data: { status: 10 },reset:true });
        },
        getFulfilledOrders: function () {
            this.updatePageNav(this.fulfilledOrders);
            this.fulfilledOrders.fetch({data: { status: 50 },reset:true });
        },
        render: function () {
            if(this.$(currentTab).parent().hasClass('active')){
                this.$(currentTab).trigger('show.bs.tab');
            } else {
                this.$(currentTab).tab('show');
            }
        }
    }))({el: $("#view-vvip-dashboard")});

    var options = { };
    MeiweiApp.Pages.VvipCenter.go(options);
});
