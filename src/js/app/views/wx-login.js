$(function () {
    var RegisterStreamItem = MeiweiApp.ModelView.extend({
        className: 'register-stream-item',
        template: TPL['register-stream-item']
    });

    var RegisterStream = MeiweiApp.CollectionView.extend({
        ModelView: RegisterStreamItem
    });

    var WeixinBindingInfo = MeiweiApp.Model.extend({
        url: '/weixin/binding'
    });

    var bindingWx = function () {
        var authToken = MeiweiApp.TokenAuth.get();
        var wxBindingInfo = new WeixinBindingInfo({
            token: authToken,
            openid: weixinInfo.openid
        });
        wxBindingInfo.save(
            {}, {
                success: function () {
                    localStorage.setItem('wxuid', weixinInfo.openid);
                    var href = 'http://mobile.clubmeiwei.com/weixin/weixin.html?showwxpaytitle=1#regAuth/' + authToken + '/member';
                    location.href = href;
                },
                error: function () {
                    //location.href = "/member/";
                }  
            }
        );
    };

    var MemberSigninForm = MeiweiApp.View.extend({
        events: { 'submit': 'signin' },
        signin: function (e) {
            if (e.preventDefault) e.preventDefault();
            var username = this.$('input[name=username]').val();
            var password = this.$('input[name=password]').val();
            var self = this;
            if (username.length > 0 && password.length > 0) {
                MeiweiApp.me.login({ username: username, password: password }, {
                    success: function () {
                        bindingWx();
                    },
                    error: function (model, xhr, options) {
                        self.displayError(self.$el, xhr.responseText);
                    }
                });
            } else {
                this.$('.non_field_errors').html('请输入用户名和密码！');
            }
        }
    });

    var MemberSignupForm = MeiweiApp.View.extend({
        events: { 'submit': 'signup' },
        login: function (username, password) {
            MeiweiApp.me.login({ username: username, password: password }, {
                success: function() {
                    bindingWx();
                }
            });
        },
        signup: function (e) {
            if (e.preventDefault) e.preventDefault();
            var username = this.$('input[name=username]').val() || null;
            var password = this.$('input[name=password]').val() || null;
            var passwordConfirm = this.$('input[name=password-confirm]').val() || null;
            if (password != passwordConfirm) {
                this.$('.non_field_errors').html(MeiweiApp._('两次密码输入不一致，请重新输入。'));
            } else if (username && password) {
                var onLoginSuccess = this.onLoginSuccess;
                var onLoginFail = this.onLoginFail;
                var self = this;
                MeiweiApp.me.register({username: username, password: password}, {
                    success: function () {
                        self.login(username, password);
                    },
                    error: function (model, xhr, options) {
                        self.displayError(self.$el, xhr.responseText);
                    }
                });
            } else {
                this.$('.non_field_errors').html('请输入用户名和密码！');
            }
        }
    });

    MeiweiApp.Pages.MemberLogin = new (MeiweiApp.PageView.extend({
        events: {
            'show.bs.tab .toggle-sign-in a': 'toggleSignIn',
            'show.bs.tab .toggle-sign-up a': 'toggleSignUp'
        },
        initPage: function () {
            this.recommend = new MeiweiApp.Models.Recommend({id: 5});
            this.views = {
                stream: new RegisterStream({
                    collection: this.recommend.items,
                    el: this.$('.stream')
                }),
                signinForm: new MemberSigninForm({ el: this.$('#sign-in-form') }),
                signupForm: new MemberSignupForm({ el: this.$('#sign-up-form') })
            };
        },
        toggleSignIn: function () {
            this.$('.toggle-sign-in').addClass('hide');
            this.$('.toggle-sign-up').removeClass('hide');
        },
        toggleSignUp: function () {
            this.$('.toggle-sign-up').addClass('hide');
            this.$('.toggle-sign-in').removeClass('hide');
        },
        render: function () {
            this.recommend.fetch({reset: true});
            if (this.options.status == 'signin') {
                this.$('.toggle-sign-in a').tab('show');
            } else {
                this.$('.toggle-sign-up a').tab('show');
            }
        }
    }))({el: $("#view-member-login")});

    MeiweiApp.Pages.MemberLogin.go({status: serverData.status});

//    document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
//        WeixinJSBridge.invoke('getBrandWCPayRequest', {
//            "appId": 'wxf8b4f85f3a794e77', //公众号名称，由商户传入
//            "timeStamp": '1395297502', //时间戳
//            "nonceStr": '532a82f5b13cf', //随机串
//            "package": 'bank_type%3DWX%26body%3D%E4%B8%AD%E6%96%87%26fee_type%3D1%26input_charset%3DGBK%26notify_url%3Dhttp%3A//www.linauror.com/wechat/notify.php%3Fa%3D6%26b%3D4%26out_trade_no%3D90020140313000002%26partner%3D1900000109%26spbill_create_ip%3D192.168.1.7%26total_fee%3D19440.0%26key%3D8934e7d15453e97507ef794cf7b0519d&sign=CC0B2B381B1B8CD84A5E67642A0CA2F1',//扩展包
//            "signType": 'SHA1', //微信签名方式:1.sha1
//            "paySign": '706f69b083f4eca8d5b04c2a344384d6678735c8' //微信签名
//        }, function (res) {
//            if (res.err_msg == "get_brand_wcpay_request:ok") {
//                window.location.href = 'http://www.linauror.com/wechat/success.php';
//            } else {
//                // 这里是取消支付或者其他意外情况，可以弹出错误信息或做其他操作
//                alert(res.err_msg);
//            }
//        });
//    }, false)


});

