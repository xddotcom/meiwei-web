$(function () {
    currentTab = '#nav-orders-new';
    MeiweiApp.Router.route('new', function() {currentTab = '#nav-orders-new';});
    MeiweiApp.Router.route('pending', function() {currentTab = '#nav-orders-pending';});
    MeiweiApp.Router.route('fulfilled', function() {currentTab = '#nav-orders-fulfilled';});
    Backbone.history.start();
    var CurrentRestaurantForm = MeiweiApp.ModelView.extend({
        events: {
            'click .restaurant-submit': 'submitRestaurantUpdate'
        },
        submitRestaurantUpdate: function () {
            var params = [this.$('[name=fullname]').val(), this.$('[name=fullnameen]').val(),
                this.$('[name=address]').val(), this.$('[name=addressen]').val(),
                this.$('[name=workinghour]').val(), this.$('[name=workinghouren]').val(),
                this.$('[name=description]').val(), this.$('[name=descriptionen]').val(),
                this.$('[name=discount]').val(), this.$('[name=discounten]').val(),
                this.$('[name=parking]').val(), this.$('[name=parkingen]').val()];
            var _id = this.model.get('id');
            this.model.clear();
            this.model.set({
                id: _id,
                fullname: { chinese: params[0], english: params[1] },
                address: { chinese: params[2], english: params[3] },
                workinghour: { chinese: params[4], english: params[5] },
                description: { chinese: params[6], english: params[7] },
                discount: { chinese: params[8], english: params[9] },
                parking: { chinese: params[10], english: params[11] }
            });
            this.model.save({}, {success: function () {
                this.$('.info-text').html('餐厅信息修改成功！');
            }});
        },
        render: function () {
            var resp = this.model.toJSON();
            if (resp.fullname) {
                var params = [resp.fullname.chinese, resp.fullname.english,
                    resp.address.chinese, resp.address.english,
                    resp.workinghour.chinese, resp.workinghour.english,
                    resp.description.chinese, resp.description.english,
                    resp.discount.chinese, resp.discount.english,
                    resp.parking.chinese, resp.parking.english];
                var inputs = [this.$('[name=fullname]').val(params[0]), this.$('[name=fullnameen]').val(params[1]),
                    this.$('[name=address]').val(params[2]), this.$('[name=addressen]').val(params[3]),
                    this.$('[name=workinghour]').val(params[4]), this.$('[name=workinghouren]').val(params[5]),
                    this.$('[name=description]').val(params[6]), this.$('[name=descriptionen]').val(params[7]),
                    this.$('[name=discount]').val(params[8]), this.$('[name=discounten]').val(params[9]),
                    this.$('[name=parking]').val(params[10]), this.$('[name=parkingen]').val(params[11])];
            }
            return this;
        }
    });

    var RestaurantList = MeiweiApp.CollectionView.extend({
        events: {
            'change select.restaurants-select': 'renderRestaurant'
        },
        ModelView: MeiweiApp.ModelView.extend({
            tagName: "option",
            template: Mustache.compile("{{ fullname.chinese }}"),
            render: function () {
                var attrs = this.model ? this.model.toJSON() : {};
                this.$el.html(this.template(attrs));
                this.$el.val(this.model.cid);
                MeiweiApp.initLang(this.$el);
                return this;
            }
        }),
        renderRestaurant: function () {
            MeiweiApp.Pages.StaffOrderList.currentRestaurant.set(
                this.restaurants.get(this.$('.restaurants-select').val()).toJSON()
            )
            this.$('.info-text').html('');
        }
    });

    var NewOrderList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: "well order-list-item",
            template: TPL['staff-orders-new'],
            events: {
                'click .confirm-new-order': 'newOrderConfirm',
                'click .cancel-new-order': 'newOrderCancel'
            },
            newOrderConfirm: function (e) {
                this.model.confirm({success: function () {
                    MeiweiApp.Pages.StaffOrderList.getNewOrders();
                }});
            },
            newOrderCancel: function (e) {
                if (e.preventDefault) e.preventDefault();
                this.model.cancel({success: function () {
                    MeiweiApp.Pages.StaffOrderList.getNewOrders();
                }});
            }
        })
    });

    var PendingOrderList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: "well order-list-item",
            template: TPL['staff-orders-pending'],
            events: {
                'click .confirm-amount': 'saveAmount',
                'click .cancel-amount': 'cancelAmount'
            },
            saveAmount: function (e) {
                if (e.preventDefault) e.preventDefault();
                var amount = parseFloat(this.$('input').val());
                if(amount) {
                    this.model.save_amount(amount, {success: function () {
                    MeiweiApp.Pages.StaffOrderList.getPendingOrders();
                }});
                }
            },
            cancelAmount: function (e) {
                if (e.preventDefault) e.preventDefault();
                this.model.cancel({success: function () {
                    MeiweiApp.Pages.StaffOrderList.getPendingOrders();
                }});
            }
        })
    });

    var FulfilledOrderList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: "well order-list-item",
            template: TPL['staff-orders-fulfilled']
        })
    });

    MeiweiApp.Pages.StaffOrderList = new (MeiweiApp.PageView.extend({
        events: {
            'show.bs.tab #nav-orders-new': 'getNewOrders',
            'show.bs.tab #nav-orders-pending': 'getPendingOrders',
            'show.bs.tab #nav-orders-fulfilled': 'getFulfilledOrders',
            'show.bs.tab #nav-restaurant-detail': 'getRestaurants'
        },
        initPage: function () {
            this.newOrders = new MeiweiApp.Collections.StaffOrders();
            this.pendingOrders = new MeiweiApp.Collections.StaffOrders();
            this.fulfilledOrders = new MeiweiApp.Collections.StaffOrders();
            this.restaurants = new MeiweiApp.Collections.StaffRestaurants();
            this.currentRestaurant = new MeiweiApp.Models.StaffRestaurant();
            this.views = {
                newOrderList: new NewOrderList({
                    collection: this.newOrders,
                    el: this.$('#tab-orders-new')
                }),
                pendingOrderList: new PendingOrderList({
                    collection: this.pendingOrders,
                    el: this.$('#tab-orders-pending')
                }),
                fulfilledOrderList: new FulfilledOrderList({
                    collection: this.fulfilledOrders,
                    el: this.$('#tab-orders-fulfilled')
                }),
                restaurantList: new RestaurantList({
                    collection: this.restaurants,
                    el: this.$('.restaurants-select')
                }),
                currentRestaurantForm: new CurrentRestaurantForm({
                    model: this.currentRestaurant,
                    el: this.$('.restaurant-item form')
                })
            };
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
            this.initPageNav(this, this.newOrders);
        },
        getNewOrders: function () {
            this.updatePageNav(this.newOrders);
            this.newOrders.fetch({ data: { status: 'new' } });
        },
        getPendingOrders: function () {
            this.updatePageNav(this.pendingOrders);
            this.pendingOrders.fetch({ data: { status: 'pending' } });
        },
        getFulfilledOrders: function () {
            this.updatePageNav(this.fulfilledOrders);
            this.fulfilledOrders.fetch({ data: { status: 'fulfilled' } });
        },
        getRestaurants: function () {
            this.restaurants.fetch();
        },
        render: function () {
            console.log(currentTab)
            if(this.$(currentTab).parent().hasClass('active')){
                this.$(currentTab).trigger('show.bs.tab');
            } else {
                this.$(currentTab).tab('show');
            }
        }
    }))({el: $("#view-staff-dashboard")});

    var options = {};
    MeiweiApp.Pages.StaffOrderList.go(options);

});
