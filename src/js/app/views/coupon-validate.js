$(function () {
    MeiweiApp.Pages.ProductDetailPage = new (MeiweiApp.PageView.extend({
        events: {
            'submit #validate-form': 'submitValidate'
        },
        initPage: function () {
            _.bindAll(this, 'submitValidate');
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
        },

        submitValidate: function (e) {
            var self = this;
            var couponNo = this.$('input[name=coupon_no]').val();
            if (!couponNo) {
                return;
            }
            var validate = new MeiweiApp.Models.Coupon({coupon_no:couponNo});
            validate.save({}, {
                success: function () {
                    self.$('input[name=coupon_no]').val('');
                    MeiweiApp.displayAlert(MeiweiApp._('Coupon Confirmed Success'));
                },
                error: function (model, xhr, options) {
                    self.displayError(self.$('form'), xhr.responseText);
                }
            });
        },
        render: function () {

        }

    }))({el: $("#view-coupon-validate")});
    var options = {

    };
    MeiweiApp.Pages.ProductDetailPage.go(options);
});
