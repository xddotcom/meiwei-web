$(function () {
    var INSTANCE = {
        concierges: [
            {image: '/assets/img/home/concierges/1.jpg'},
            {image: '/assets/img/home/concierges/2.jpg'},
            {image: '/assets/img/home/concierges/3.jpg'},
            {image: '/assets/img/home/concierges/4.jpg'},
            {image: '/assets/img/home/concierges/5.jpg'},
            {image: '/assets/img/home/concierges/7.jpg'},
            {image: '/assets/img/home/concierges/8.jpg'},
            {image: '/assets/img/home/concierges/6.jpg'}
        ],
        resaurants1: [
            {image: '/assets/img/home/restaurants/1.jpg', recommend_id: 19},
            {image: '/assets/img/home/restaurants/2.jpg', recommend_id: 5},
            {image: '/assets/img/home/restaurants/3.jpg', recommend_id: 4}
        ],
        resaurants2: [
            {image: '/assets/img/home/restaurants/4.jpg', recommend_id: 25},
            {image: '/assets/img/home/restaurants/5.jpg', recommend_id: 16},
            {image: '/assets/img/home/restaurants/6.jpg', recommend_id: 24}
        ],
        resaurants3: [
            {image: '/assets/img/home/restaurants/7.jpg', recommend_id: 30},
            {image: '/assets/img/home/restaurants/8.jpg', recommend_id: 28},
            {image: '/assets/img/home/restaurants/9.jpg', recommend_id: 29}
        ],
        resaurants4: [
            {image: '/assets/img/home/restaurants/10.jpg', recommend_id: 8},
            {image: '/assets/img/home/restaurants/11.jpg', recommend_id: 18},
            {image: '/assets/img/home/restaurants/12.jpg', recommend_id: 17}
        ],
        stories: [
            {
                image: '/assets/img/home/stories/1.jpg',
                title: '<i class="fa fa-star-o"></i> 美位·上海站开启',
                source: 'http://blog.clubmeiwei.com/?p=15'
            },
            {
                image: '/assets/img/home/stories/2.jpg',
                title: '<i class="fa fa-heart-o"></i> 白色情人节之夜',
                source: 'http://blog.clubmeiwei.com/?p=35'
            },
            {
                image: '/assets/img/home/stories/3.jpg', 
                title: '<i class="fa fa-glass"></i> 庄源开业晚宴',
                source: 'http://blog.clubmeiwei.com/?p=24'
            }
        ],
        cooperations: [
            {image: '/assets/img/home/cooperations/1.jpg'},
            {image: '/assets/img/home/cooperations/2.jpg'},
            {image: '/assets/img/home/cooperations/3.jpg'},
            {image: '/assets/img/home/cooperations/4.jpg'},
            {image: '/assets/img/home/cooperations/5.jpg'},
            {image: '/assets/img/home/cooperations/6.jpg'}
        ]
    };

    var ConciergesView = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'magicbox-item',
            template: Mustache.compile('<a href="/product/cake/"><img src="{{image}}"></a>'),
            render: function() {
                MeiweiApp.ModelView.prototype.render.call(this);
                if (this.model.get('cover')) {
                    this.$el.addClass('cover');
                    this.$el.css('background', this.model.get('cover_bg'));
                }
                return this;
            }
        }),
        addAll: function () {
            MeiweiApp.CollectionView.prototype.addAll.call(this);
            return this;
        }
    });

    var RestaurantsView = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'carousel-inner-item',
            template: Mustache.compile('<a href="/restaurant/search/?r={{recommend_id}}">' +
                '<img class="img-responsive" src="{{image}}">' +
                '</a>')
        })
    });

    var StoriesView = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'story-item',
            template: TPL['homepage-story-item']
        })
    });

    var CoorperationsView = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'brand-logo col-sm-2 col-xs-4',
            template: Mustache.compile('<img class="img-responsive" src="{{image}}">')
        })
    });

    MeiweiApp.Pages.Home = new (MeiweiApp.PageView.extend({
        events: {
            'click .section-nav > ul > li > a': 'scrollToSection',
            'activate.bs.scrollspy .section-nav': 'sectionOnActive'
        },
        initPage: function() {
            this.restaurants1 = new MeiweiApp.Collection();
            this.restaurants2 = new MeiweiApp.Collection();
            this.restaurants3 = new MeiweiApp.Collection();
            this.restaurants4 = new MeiweiApp.Collection();
            this.concierges = new MeiweiApp.Collection();
            this.stories = new MeiweiApp.Collection();
            this.cooperations = new MeiweiApp.Collection();
            this.views = {
                concierges: new ConciergesView({
                    collection: this.concierges, el: this.$('.concierges .magicbox-inner')
                }),
                restaurants: [
                    new RestaurantsView({
                        collection: this.restaurants1, el: this.$('.restaurants .item')[0]
                    }),
                    new RestaurantsView({
                        collection: this.restaurants2, el: this.$('.restaurants .item')[1]
                    }),
                    new RestaurantsView({
                        collection: this.restaurants3, el: this.$('.restaurants .item')[2]
                    }),
                    new RestaurantsView({
                        collection: this.restaurants4, el: this.$('.restaurants .item')[3]
                    })
                ],
                stories: new StoriesView({
                    collection: this.stories, el: this.$('.stories .story-row')
                }),
                cooperations: new CoorperationsView({
                    collection: this.cooperations, el: this.$('.cooperations>.container>.row')
                })
            };
            this.initScroll();
        },
        sectionOnActive: function (e) {
            var anchor = e.target;
            var href = $(anchor).find('a').attr('href');
            if (href == '#section-concierges') {
                this.$('.img-wine').removeClass('rotate-image');
                this.$('#values-carousel-0').removeClass('values-carousel-transition');
            } else {
                this.$('.img-wine').addClass('rotate-image');
                this.$('#values-carousel-0').addClass('values-carousel-transition');
            }

            if (href == '#section-gift') {
                this.$('.img-heart').removeClass('rotate-image-left');
                this.$('#values-carousel-1').removeClass('values-carousel-transition');
            } else {
                this.$('.img-heart').addClass('rotate-image-left');
                this.$('#values-carousel-1').addClass('values-carousel-transition');
            }

            if (href == '#section-vvip') {
//                this.$('.img-vvip').removeClass('rotate-image');
                this.$('#values-carousel-2').removeClass('values-carousel-transition');
            } else {
//                this.$('.img-vvip').addClass('rotate-image');
                this.$('#values-carousel-2').addClass('values-carousel-transition');
            }
        },
        initScroll: function() {
            $('body').scrollspy({
                target: '.section-nav',
                offset: 300
            });
        },
        scrollToSection: function(e) {
            if (e.preventDefault) e.preventDefault();
            var anchor = e.target;
            var $sec = $($(anchor).attr('href'));
            var offset = $sec.position().top;
            $('body').animate({
                scrollTop: offset
            }, 300, "linear");
        },
        render: function() {
//            this.$('#homepage-carousel').carousel({interval: 3000});
            this.$('#restaurants-carousel').carousel({interval: 3000});
            this.$('#values-carousel-0').carousel({interval: 3000});
            this.$('#values-carousel-1').carousel({interval: 3000});
            this.$('#values-carousel-2').carousel({interval: 3000});
            this.restaurants1.reset(INSTANCE.resaurants1);
            this.restaurants2.reset(INSTANCE.resaurants2);
            this.restaurants3.reset(INSTANCE.resaurants3);
            this.restaurants4.reset(INSTANCE.resaurants4);
//            this.concierges.reset(INSTANCE.concierges);
//            this.stories.reset(INSTANCE.stories);
//            this.cooperations.reset(INSTANCE.cooperations);
            $('body').scrollspy('refresh');
        }
    }))({el: $("#view-home")});

    MeiweiApp.Pages.Home.go();

});
