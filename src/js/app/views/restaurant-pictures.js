$(function () {

    var PicturesList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: Mustache.compile('<div class="ar ar23"></div>' +
                '<div class="ar-inner"><img src="{{path}}"></div>'),
            className: 'picture ar-outer',
            events: { 'click': 'toggleView' },
            toggleView: function () {
                this.$el.toggleClass('toggleView');
            }
        })
    });

    MeiweiApp.Pages.RestaurantPictures = new (MeiweiApp.PageView.extend({
        initPage: function () {
            this.restaurant = new MeiweiApp.Models.Restaurant();
            this.pictures = new MeiweiApp.Collections.Pictures();
            this.restaurant.on('change', function () {
                this.$('.fullname').html(this.restaurant.get('fullname'))
                    .attr('href', '/restaurant/' + this.restaurant.id);
                this.pictures.reset(this.restaurant.get('pictures'));
            }, this);
            this.views = {
                picturesList: new PicturesList({ collection: this.pictures, el: this.$('.picture-list') })
            };
        },
        render: function () {
            if (this.options.restaurant) {
                this.restaurant.set(this.options.restaurant);
            } else if (this.options.restaurantId) {
                this.restaurant.clear({silent: true});
                this.restaurant.set({id: this.options.restaurantId}, {silent: true});
                this.restaurant.fetch();
            }
        }
    }))({el: $("#view-restaurant-pictures")});

    MeiweiApp.Pages.RestaurantPictures.go(serverData);
});
