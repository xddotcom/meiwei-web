$(function () {
    var MapView = MeiweiApp.ModelView.extend({
        initModelView: function() {
            _.bindAll(this, 'showMap');
        },
        showMap: function () {
            if (!window.AMap) {
                window.initializeMap = this.showMap;
                $.getScript("http://webapi.amap.com/maps?v=1.2&key=88079b446671c954e1de335141228c28&callback=initializeMap");
                return;
            }
            var position = new AMap.LngLat(this.coordinate.longitude / 1e5, this.coordinate.latitude / 1e5);
            var mapObj = this.map = new AMap.Map('map-canvas', {
                center: position,
                continuousZoomEnable: true, level: 15, zooms: [3, 17], touchZoom: true
            });

            var marker = new AMap.Marker({ //自定义构造AMap.Marker对象
                map:mapObj,
                position: position,
                offset: new AMap.Pixel(-10,-34),
                icon: "http://webapi.amap.com/images/0.png"
            });
        },
        render: function () {
            this.$('.address').html(this.model.get('address'));
            var coordinate = this.model.get('coordinate');
            if (!coordinate) return;
            this.coordinate = coordinate;
            this.showMap();
            return this;
        }
    });

    var RestaurantDetail = MeiweiApp.ModelView.extend({
        events: { 'click .hero img': 'viewPictures' },
        template: TPL['restaurant-detail'],
        viewPictures: function () {
            window.location.href = '/restaurant/' + this.model.id + '/pictures/';
        },
        render: function () {
            MeiweiApp.ModelView.prototype.render.call(this);
            var firstItem = this.$('.carousel .item')[0];
            if (firstItem) {
                $(firstItem).addClass('active');
            } else {
                this.$('.hero').addClass('hidden');
            }

        }
    });

    var QuickOrder = MeiweiApp.ModelView.extend({
        template: TPL['restaurant-quick-order'],
        initModelView: function () {
            _.bindAll(this, 'renderHourList');
        },
        renderOrderDate: function () {
            var now = new Date(), tomorrow = new Date();
            tomorrow.setDate(now.getDate() + 3);
            tomorrow.setMinutes(0);
            if (MeiweiApp.isMobile) {
                var str = tomorrow.toISOString();
                this.$('input[name=orderdate]').attr('value', str.slice(0, 10));
                this.$('input[name=orderdate]').attr('type', 'date');
                this.$('input[name=orderdate]').on('change', this.renderHourList);
            } else {
                this.$('input[name=orderdate]').datetimepicker({
                    minView: 'month',
                    todayBtn: true,
                    autoclose: true,
                    todayHighlight: true,
                    language: 'zh-CN',
                    format: "yyyy-mm-dd"
                }).datetimepicker('setStartDate', now)
                    .datetimepicker('update', tomorrow)
                    .on('changeDate', this.renderHourList);
            }
        },
        renderHourList: function () {
            if (!this.hours) {
                this.hours = new MeiweiApp.Models.Hour();
                this.hours.fetch({
                    url: this.model.get('hours'),
                    success: this.renderHourList
                });
            }
            var ymd = this.$('input[name=orderdate]').val().split('-');
            var day = (ymd.length == 3 ? (new Date(ymd[0], ymd[1] - 1, ymd[2])).getDay().toString() : '0');
            var $select = this.$('select[name=ordertime]');
            $select.empty();
            var hour = this.hours.get(day);
            if (hour && hour.length > 0) {
                for (var i = 0; i < hour.length; i++) {
                    var item = hour[i];
                    var $option = $('<option></option>').val(item[0]).html(item[0] + ' ' + item[1]);
                    $select.append($option);
                }
            }
        },
        render: function () {
            MeiweiApp.ModelView.prototype.render.call(this);
            this.renderOrderDate();
            this.renderHourList();
        }
    });

    MeiweiApp.Pages.RestaurantDetail = new (MeiweiApp.PageView.extend({
        initPage: function () {
            this.restaurant = new MeiweiApp.Models.Restaurant();
            this.views = {
                mapView: new MapView({ model: this.restaurant, el: this.$('.restaurant-map') }),
                detailView: new RestaurantDetail({ model: this.restaurant, el: this.$('.restaurant-detail') }),
                quickOrder: new QuickOrder({ model: this.restaurant, el: this.$('.quick-order') })
            };
        },
        render: function () {
            if (this.options.restaurant) {
                this.restaurant.set(this.options.restaurant);
            } else if (this.options.restaurantId) {
                this.restaurant.clear({silent: true});
                this.restaurant.set({id: this.options.restaurantId}, {silent: true});
                this.restaurant.fetch();
            }
        }
    }))({el: $("#view-restaurant-detail")});

    MeiweiApp.Pages.RestaurantDetail.go(serverData);
});
