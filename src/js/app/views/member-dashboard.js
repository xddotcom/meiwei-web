$(function () {
    currentTab = '#nav-order-pending';
    MeiweiApp.Router.route('pending', function() {currentTab = '#nav-order-pending';});
    MeiweiApp.Router.route('fulfilled', function() {currentTab = '#nav-order-fulfilled';});
    MeiweiApp.Router.route('profile', function() {currentTab = '#nav-member-profile';});
    MeiweiApp.Router.route('credit', function() {currentTab = '#nav-member-credit';});
    MeiweiApp.Router.route('favorite', function() {currentTab = '#nav-member-favorite';});
    MeiweiApp.Router.route('contact', function() {currentTab = '#nav-member-contact';});
    MeiweiApp.Router.route('product', function() {currentTab = '#nav-member-product';});
    Backbone.history.start();

    var PendingOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['member-orders-pending'],
            className: "thumbnail order-list-item",
            events: {
                "click .order-cancel": "cancelOrder"
            },
            cancelOrder: function (e) {
                this.model.cancel();
                this.model.collection.remove(this.model);
            },
            initModelView: function () {
                var orderDateTime = moment(this.model.get('orderdate') + ' ' + this.model.get('ordertime')).format('LLL');
                this.model.set('datetime', orderDateTime);
                if(this.model.get('payment_order')) {
                    var paymentStatus = this.model.get('payment_order').status;
                    if (paymentStatus == 50) {
                        this.model.set('payment_status', false);
                    } else {
                        this.model.set('payment_status', true);
                    }
                }
            }
        })
    });

    var FulfilledOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['member-orders-fulfilled'],
            className: "thumbnail order-list-item",
            initModelView: function () {
                var orderDateTime = moment(this.model.get('orderdate') + ' ' + this.model.get('ordertime')).format('LLL');
                this.model.set('datetime', orderDateTime);
            }
        })

    });

    var MemberProfileForm = MeiweiApp.ModelView.extend({
        events: {
            'submit .profile-form': 'updateProfile',
            'submit .password-form': 'updatePassword'
        },
        template: TPL['member-profile-form'],
        updateProfile: function (e) {
            if (e.preventDefault) e.preventDefault();
            MeiweiApp.me.profile.set({
                nickname: this.$('input[name=nickname]').val() || null,
                email: this.$('input[name=email]').val() || null,
                mobile: this.$('input[name=mobile]').val() || null,
                sexe: this.$('select[name=sexe]').val() || null,
                birthday: this.$('input[name=birthday]').val() || null
            });
            var self = this;
            MeiweiApp.me.profile.save({}, {
                success:function(){
                    MeiweiApp.displayAlert(MeiweiApp._('Profile Update Success'));
                },
                error: function (model, xhr, options) {
                    self.displayError(self.$('.profile-form'), xhr.responseText);
                }
            });
        },
        updatePassword: function (e) {
            if (e.preventDefault) e.preventDefault();
            var password = this.$('input[name=password]').val() || null;
            var passwordConfirm = this.$('input[name=password-confirm]').val() || null;
            if (password != passwordConfirm) {
                this.$('.info-text').html(MeiweiApp._("Password doesn't match."));
            } else {
                var self = this;
                MeiweiApp.me.changePassword(password, {
                    success:function(){
                        MeiweiApp.displayAlert(MeiweiApp._('Password Update Success'));
                    },
                    error: function (model, xhr, options) {
                        self.displayError(self.$('.password-form'), xhr.responseText);
                    }
                });
            }
        }
    });

    var CreditList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            template: TPL['credit-list-item'],
            className: 'well well-sm'
        })
    });

    var FavoriteList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'restaurant-list-item',
            template: TPL['favorite-list-item'],
            events: { 'click .delete-button': 'deleteFav','click > header': 'viewRestaurant' },
            viewRestaurant: function() {
                window.location.href = '/restaurant/' + this.model.id;
            },
            deleteFav: function (e) {
                this.model.destroy();
            }
        })
    });

    var ContactList = MeiweiApp.CollectionView.extend({
        ModelView: MeiweiApp.ModelView.extend({
            className: 'well well-sm',
            template: TPL['contact-list-item'],
            events: { 'click .delete-button': 'deleteContact' },
            deleteContact: function () {
                this.model.destroy();
            }
        })
    });

    var ProductOrderList = MeiweiApp.CollectionView.extend({
        initCollectionView: function () {
            if (MeiweiApp.IE8_Compatible) {
                this.listenTo(this.collection, 'reset add remove', this.fixOddEven);
            }
        },
        fixOddEven: function () {
            this.$el.find('.order-list-item').removeClass('even');
            this.$el.find('.order-list-item:odd').addClass('even');
        },
        ModelView: MeiweiApp.ModelView.extend({
            className: "thumbnail order-list-item",
            template: TPL['product-order-list-item'],
            events: {
                'click .order-cancel': 'deleteOrder',
                'click .order-payable': 'payOrder'
            },
            deleteOrder: function () {
                this.model.cancel();
                this.model.collection.remove(this.model);
            },
            initModelView: function () {
                var self = this;
                var dateLocal = moment(this.model.get('time_created')).format('LLL');
                var orderTime = moment(this.model.get('attributes').datetime).format('LLL');
                this.model.get('attributes').datetime = orderTime;
                this.model.set('time_created', dateLocal);

                var orderType = this.model.get('order_type');
                if (orderType == 20) {
                    this.template = TPL['product-order-list-item'];
                } else if (orderType == 30) {
                    this.template = TPL['anshifu-order-list-item'];
                    var assignTime = moment(this.model.get('attributes').assign_time).format('LLL');
                    this.model.get('attributes').assign_time = assignTime;
                } else if (orderType == 40) {
                    this.template = TPL['airport-order-list-item'];
                    var arrival = false;
                    if (this.model.get('attributes').service_type == 'arrival') {
                        arrival = true;
                    } else {
                        arrival = false;
                    }
                    var guests = JSON.parse(this.model.get('attributes').guests);
                    this.model.get('attributes').guests = guests;
                    this.model.set('arrival', arrival);

                } else if (orderType == 50) {
                    this.template = TPL['pingan-order-list-item'];
                } else if (orderType == 60) {
                    this.template = TPL['restaurant-reservation-item'];
                }

                if(this.model.get('payment')) {
                    var paymentStatus = this.model.get('payment').status;
                    if (paymentStatus == 50) {
                        this.model.set('payment_status', false);
                    } else {
                        this.model.set('payment_status', true);
                    }
                }

                var status = this.model.get('status');
                if (status == 0) {
                    this.model.set('order_new', true);
                } else if (status == 10) {
                    this.model.set('order_confirmed', true);
                } else if (status == 50) {
                    this.model.set('order_finished', true);
                } else if (status == 99) {
                    this.model.set('order_cancelled', true);
                }
            }
        })
    });

    MeiweiApp.Pages.MemberCenter = new (MeiweiApp.PageView.extend({
        events: {
            'show.bs.tab #nav-member-profile': 'fetchMe',
            'show.bs.tab #nav-order-pending': 'getPendingOrders',
            'show.bs.tab #nav-order-fulfilled': 'getFulfilledOrders',
            'show.bs.tab #nav-member-credit': 'getMemberCredit',
            'show.bs.tab #nav-member-favorite': 'getMemberFavorite',
            'show.bs.tab #nav-member-contact': 'getMemberContact',
            'show.bs.tab #nav-member-product': 'getMemberProductOrder'
        },
        initPage: function () {
            this.pendingOrders = new MeiweiApp.Collections.Orders();
            this.fulfilledOrders = new MeiweiApp.Collections.Orders();
            this.contacts = new MeiweiApp.Collections.Contacts();
            this.favorites = new MeiweiApp.Collections.Favorites();
            this.credits = new MeiweiApp.Collections.Credits();
            this.productOrders = new MeiweiApp.Collections.GenericOrders();
//            this.productOrders.comparator = function (model) {
//                return model.get('order_type');
//            };
            this.views = {
                pendingOrderList: new PendingOrderList({
                    collection: this.pendingOrders,
                    el: this.$('#tab-order-pending')
                }),
                fulfilledOrderList: new FulfilledOrderList({
                    collection: this.fulfilledOrders,
                    el: this.$("#tab-order-fulfilled")
                }),
                profileForm: new MemberProfileForm({
                    model: MeiweiApp.me.profile,
                    el: this.$('#tab-member-profile')
                }),
                memberCredits: new CreditList({
                    collection: this.credits,
                    el: this.$('#tab-member-credit')
                }),
                memberFavorites: new FavoriteList({
                    collection: this.favorites,
                    el: this.$('#tab-member-favorite')
                }),
                memberContacts: new ContactList({
                    collection: this.contacts,
                    el: this.$('#tab-member-contact')
                }),
                memberProductOrders: new ProductOrderList({
                    collection: this.productOrders,
                    el: this.$('#tab-member-product')
                })
            };
            this.initPageNav(this, this.pendingOrders);
            this.listenTo(MeiweiApp.me, 'login', this.refresh);
        },
        fetchMe: function () {
            MeiweiApp.me.fetch();
        },
        getPendingOrders: function () {
            this.updatePageNav(this.pendingOrders);
            this.pendingOrders.fetch({data: { status: 'pending' } });
        },
        getFulfilledOrders: function () {
            this.updatePageNav(this.fulfilledOrders);
            this.fulfilledOrders.fetch({data: { status: "fulfilled" } });
        },
        getMemberCredit: function () {
            this.updatePageNav(this.credits);
            this.credits.fetch();
        },
        getMemberFavorite: function () {
            this.updatePageNav(this.favorites);
            this.favorites.fetch();
        },
        getMemberContact: function () {
            this.updatePageNav(this.contacts);
            this.contacts.fetch();
        },
        getMemberProductOrder: function () {
            this.updatePageNav(this.productOrders);
            this.productOrders.fetch({
                reset: true,
                data: {}
            });
        },
        render: function () {
            if(this.$(currentTab).parent().hasClass('active')){
                this.$(currentTab).trigger('show.bs.tab');
            } else {
                this.$(currentTab).tab('show');
            }

        }
    }))({el: $("#view-member-dashboard")});

    var options = { };
    MeiweiApp.Pages.MemberCenter.go(options);
});
