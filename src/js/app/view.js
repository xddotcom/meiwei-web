
MeiweiApp.View = Backbone.View.extend({
    initialize: function() {
        if (this.initView) this.initView();
    },
    displayError: function($form, error) {
        try {
            error = JSON.parse(error);
            for (var name in error) {
                var $input = $form.find('[name=' + name + ']');
                if ($input.length == 0) {
                    error['non_field_errors'] = error[name];
                } else {
                    var $inputLine = $input.closest('.input-group');
                    if ($inputLine.length == 0) $inputLine = $input;
                    $inputLine.closest('.form-group').addClass('has-error');
                    $inputLine.after($('<span class="help-block field-error"></span>').text(error[name]));
                    $input.one('focus', function() {
                        var $inputLine = $(this).closest('.input-group');
                        if ($inputLine.length == 0) $inputLine = $(this);
                        $inputLine.closest('.form-group').removeClass('has-error');
                        $inputLine.siblings('.field-error').remove();
                    });
                }
            }
            if (error['non_field_errors']) {
                $form.find('.non_field_errors')
                    .text(error['non_field_errors'])
                    .closest('.form-group').addClass('has-error');
            }
            $form.one('submit', function() {
                $form.find('.has-error').removeClass('has-error');
                $form.find('.field-error').remove();
                $form.find('.non_field_errors').empty();
            });
        } catch (e) {
            $form.find('.non_field_errors').text(error || 'Error');
        }
    },
    template: Mustache.compile(""),
    renderTemplate: function(attrs, template) {
        template = template || this.template;
        this.$el.html(template(attrs || {}));
        MeiweiApp.initLang(this.$el);
        return this;
    }
});

MeiweiApp.ModelView = MeiweiApp.View.extend({
    initView: function() {
        if (this.model) {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'hide', this.hide);
        }
        if (this.initModelView) this.initModelView();
    },
    hide: function() {
        var self = this;
        this.$el.animate({
            opacity: 0
        }, 1000, function() {
            self.remove();
        });
    },
    render: function() {
        var attrs = this.model ? this.model.toJSON() : {};
        return this.renderTemplate(attrs);
    }
});

MeiweiApp.CollectionView = MeiweiApp.View.extend({
    ModelView: MeiweiApp.ModelView,
    initView: function() {
        if (this.collection) {
            this.listenTo(this.collection, 'reset', this.addAll);
            this.listenTo(this.collection, 'add', this.addOne);
            this.listenTo(this.collection, 'remove', this.removeOne);
        }
        if (this.initCollectionView) this.initCollectionView();
    },
    removeOne: function(item) {
        item.trigger('hide');
    },
    addOne: function(item) {
        var modelView = new this.ModelView({model: item});
        this.$el.append(modelView.render().el);
    },
    addAll: function(_collection, options) {
        if (options && options.previousModels) {
            _.each(options.previousModels, function(model) {
                model.trigger('hide');
            });
        }
        if (this.collection && !this.collection.isEmpty()) {
            var $list = [];
            this.collection.forEach(function(item) {
                var modelView = new this.ModelView({model: item});
                $list.push(modelView.render().el);
            }, this);
            this.$el.html($list);
        } else {
            this.renderEmptyList();
        }
    },
    renderEmptyList: function() {
        var template = Mustache.compile('<p class="text-center well" data-i18n="Nothing Here"></p>');
        this.renderTemplate({}, template);
    },
    render: function() {
        this.addAll();
        return this;
    }
});

MeiweiApp.PageView = MeiweiApp.View.extend({
    disablePage: function() {
        this.undelegateEvents();
        this.go = this.refresh = function() {};
    },
    initView: function() {
        if (!this.el) {
            this.disablePage();
            return;
        }
        this.views = {};
        _.bindAll(this, 'go', 'refresh', 'render', 'reset');
        if (this.initPage) this.initPage();
    },
    initPageNav: function(page, collection) {
        page.navOnCollection = collection;
        var fetching = false;
        page.$('.show-more').attr('data-loading-text', '<i class="fa fa-refresh fa-lg fa-spin"></i>');
        page.fetchMore = function() {
            if (fetching || !page.navOnCollection.next) return;
            fetching = true;
            page.$('.show-more').button('loading');
            setTimeout(function() {
                page.navOnCollection.fetchNext({success: page.resetNavigator, error: page.resetNavigator, remove: false});
            }, 500);
        };
        page.resetNavigator = function() {
            fetching = false;
            page.$('.show-more').button('reset');
            page.$('.show-more').toggleClass('hidden', (page.navOnCollection.next == null));
        };
        $(window).scroll(function() {
            var footerHeight = page.$('.master-footer').height() + $(window).height();
            if ($('body').height() - $(window).scrollTop() < footerHeight) {
                page.fetchMore();
            }
        });
        page.$el.on('click', '.show-more', page.fetchMore);
        page.listenTo(page.navOnCollection, 'reset', page.resetNavigator);
        page.updatePageNav = function(collection) {
            page.stopListening(page.navOnCollection);
            page.navOnCollection = collection;
            page.listenTo(page.navOnCollection, 'reset', page.resetNavigator);
            page.resetNavigator();
        }
    },
    renderNavBar: function() {
        var self = this;
        // Logo
        this.$el.on('mouseover', '.master-header .logo', function() {
            self.$('.master-header .logo-hover').addClass('hover');
        });
        this.$el.on('mouseleave', '.master-header .logo', function() {
            self.$('.master-header .logo-hover').removeClass('hover');
        });
        // Search
        var submitQuery = function(e) {
            if (e.preventDefault) e.preventDefault();
            window.location.href = '/restaurant/search/?q=' + self.$('.search-input').val();
        }
        this.$el.on('submit', '.master-header form', submitQuery);
        this.$el.on('click', '.master-header .search-btn', submitQuery);
        // Login
        var renderLoginBar = function() {
            var authToken = MeiweiApp.TokenAuth.get();
            self.$('.master-header .anonymous').toggleClass('hidden', authToken != null);
            self.$('.master-header .loggedin').toggleClass('hidden', authToken == null);
        };
        MeiweiApp.me.on('login', renderLoginBar);
        renderLoginBar();
        // Logout
        this.$el.on('click', '.master-header .logout', function() { MeiweiApp.me.logout(); });
        MeiweiApp.me.on('logout', function() { location.reload(); });
        // Language
        this.$el.on('click', '.lang-opt.en', function() { MeiweiApp.setLang('en'); });
        this.$el.on('click', '.lang-opt.zh', function() { MeiweiApp.setLang('zh'); });
    },
    go: function(options) {
        this.options = options || {};
        this.reset();
        this.renderNavBar();
        this.render();
    },
    refresh: function() {
        this.renderNavBar();
        this.render();
    },
    reset: function() {}
});

MeiweiApp.Pages.MemberLoginPopup = new (MeiweiApp.PageView.extend({
    initPage: function() {
        this.views = {
            signinForm: new (MeiweiApp.View.extend({
                events: {
                    'submit': 'signin',
                    'click .register-link': 'goToRegister'
                },
                initView: function() {
                    _.bindAll(this, 'signin', 'onSuccess', 'onError');
                },
                onSuccess: function() {
                    MeiweiApp.Pages.MemberLoginPopup.$el.modal('hide');
                },
                onError: function(model, xhr, options) {
                    this.displayError(this.$el, xhr.responseText);
                },
                signin: function(e) {
                    if (e.preventDefault) e.preventDefault();
                    var username = this.$('input[name=username]').val();
                    var password = this.$('input[name=password]').val();
                    if (username.length > 0 && password.length > 0) {
                        MeiweiApp.me.login({ username : username, password : password }, {
                            success : this.onSuccess, error : this.onError
                        });
                    }else{
                        this.$('.non_field_errors').html('请输入用户名和密码！');
                    }
                },
                goToRegister: function() {
                    localStorage.setItem('register-redirect', window.location.href);
                }
            }))({ el: this.$('form') })
        };
    },
    render: function() {
        this.$el.modal('show');
    }
}))({el: $("#dialog-member-login")});
