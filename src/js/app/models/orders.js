MeiweiApp.Models.Order = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/order/',
    parse: function (response) {
        response.ordertime = response.ordertime.slice(0, 5);
        response.editable = (response.status < 20);
        response.is_payable = false;
        response.is_prepay = false;
        if(response.payment_order) {
            if(response.payment_order.amount>0) {
                response.is_prepay = true;
            }
            if(response.payment_order.status==0) {
                response.is_payable = true;
            }
        }
        return response;
    },
    cancel: function (options) {
        options = options || {};
        var url = this.url() + 'cancel/';
        options.url = url;
        Backbone.sync('update', this, options);
    }
});

MeiweiApp.Collections.Orders = MeiweiApp.Collection.extend({
    url: MeiweiApp.configs.APIHost + '/orders/order/',
    model: MeiweiApp.Models.Order
});

MeiweiApp.Models.StaffOrder = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/staffs/order/',
    parse: function (response) {
        if (response && response.ordertime) {
            response.ordertime = response.ordertime.slice(0, 5);
            response.editable = (response.status < 20);
            response.is_payable = false;
            response.is_prepay = false;
            if(response.payment_order) {
                if(response.payment_order.amount>0) {
                    response.is_prepay = true;
                }
                if(response.payment_order.status==0) {
                    response.is_payable = true;
                }
            }
        }
        return response;
    },
    confirm: function (options) {
        options = options || {};
        var url = this.url() + 'confirm/';
        options.url = url;
        Backbone.sync('update', this, options);
    },
    save_amount: function (amount, options) {
        this.set('amount', amount)
        options = options || {};
        var url = this.url() + 'save_amount/';
        options.url = url;
        Backbone.sync('update', this, options);
    },
    cancel: function (options) {
        options = options || {};
        var url = this.url() + 'cancel/';
        options.url = url;
        Backbone.sync('update', this, options);
    }
});

MeiweiApp.Collections.StaffOrders = MeiweiApp.Collection.extend({
    url: MeiweiApp.configs.APIHost + '/staffs/order/',
    model: MeiweiApp.Models.StaffOrder
});


MeiweiApp.Models.StaffAdminOrder = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/staffs/staffadminorder/',
    parse: function (response) {
        if (response && response.ordertime) {
            response.ordertime = response.ordertime.slice(0, 5);
            response.editable = (response.status < 20);
            response.is_payable = false;
            response.is_prepay = false;
            if(response.payment_order) {
                if(response.payment_order.amount>0) {
                    response.is_prepay = true;
                }
                if(response.payment_order.status==0) {
                    response.is_payable = true;
                }
            }
        }
        return response;
    },
    confirm: function (options) {
        options = options || {};
        var url = this.url() + 'confirm/';
        options.url = url;
        Backbone.sync('update', this, options);
    },
    cancel: function (reason,options) {
        options = options || {};
        this.set('reason', reason);
        var url = this.url() + 'cancel/';
        options.url = url;
        Backbone.sync('update', this, options);
    }
});

MeiweiApp.Collections.StaffAdminOrders = MeiweiApp.Collection.extend({
    url: MeiweiApp.configs.APIHost + '/staffs/staffadminorder/',
    model: MeiweiApp.Models.StaffAdminOrder
});

MeiweiApp.Models.GenericOrder = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/genericorder/',
    payable: function (options) {
        options.url = this.url() + 'payment/';
        Backbone.sync('get', this, options);
    },
    confirm: function (options) {
        options = options || {};
        var url = this.url() + 'confirm/';
        options.url = url;
        Backbone.sync('update', this, options);
    }
});

MeiweiApp.Collections.GenericOrders = MeiweiApp.Collection.extend({
    url: MeiweiApp.configs.APIHost + '/orders/genericorder/',
    model: MeiweiApp.Models.GenericOrder
});

MeiweiApp.Models.AirportOrderCreation = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/orderairport/'
});

MeiweiApp.Models.PingAnOrderCreation = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/orderpingan/'
});

MeiweiApp.Models.ProductOrderCreation = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/orderproduct/'
});

MeiweiApp.Models.StaffAirportOrder = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/staffs/orderairport/',
    confirm: function (options) {
        options = options || {};
        var url = this.url() + 'confirm/';
        options.url = url;
        Backbone.sync('update', this, options);
    },
    complete: function (options) {
        options = options || {};
        var url = this.url() + 'complete/';
        options.url = url;
        Backbone.sync('update', this, options);
    }
});

MeiweiApp.Collections.StaffAirportOrders = MeiweiApp.Collection.extend({
    url: MeiweiApp.configs.APIHost + '/staffs/orderairport/',
    model: MeiweiApp.Models.StaffAirportOrder
});

MeiweiApp.Models.Coupon = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/orders/coupon/',
    idAttribute: 'coupon_no',
    url:function(){
        return this.urlRoot + this.get('coupon_no') + '/validate/'
    }
});