MeiweiApp.i18n = {
    'Circles': {
        zh: '全部商圈',
        en: 'Circles'
    },
    'Cuisines': {
        zh: '全部菜系',
        en: 'Cuisines'
    },
    'Next Page': {
        zh: '下一页',
        en: 'Next Page'
    },
    'Previous Page': {
        zh: '上一页',
        en: 'Previous Page'
    },
    'More...': {
        zh: '更多...',
        en: 'More...'
    },
    'Nothing Here': {
        zh: '列表为空',
        en: 'Nothing Here'
    },
    'Loading...': {
        zh: '努力加载中...',
        en: 'Loading...'
    },
    'Reserve': {
        zh: '订餐',
        en: 'Book'
    },
    'Call Us': {
        zh: '客服电话',
        en: 'Call Us'
    },
    'Mr.': {
        zh: '先生',
        en: 'Mr.'
    },
    'Ms.': {
        zh: '小姐',
        en: 'Ms.'
    },
    'Reservation Detail': {
        zh: '预订信息',
        en: 'Reservation Detail'
    },
    'Select Contact': {
        zh: '选择预订人',
        en: 'Select Contact'
    },
    'Select Seat': {
        zh: '桌位选择',
        en: 'Select Seat'
    },
    'Submit Order': {
        zh: '提交订单',
        en: 'Submit Order'
    },
    'Submit': {
        zh: '提交',
        en: 'Submit'
    },
    'Parking': {
        zh: '停车信息',
        en: 'Parking'
    },
    'Floorplan': {
        zh: '桌位',
        en: 'Floorplan'
    },
    'Member Center': {
        zh: '用户中心',
        en: 'Member Center'
    },
    'My': {
        zh: '我的',
        en: 'My '
    },
    'Recipient': {
        zh: '收件人',
        en: 'Recipient'
    },
    'Return to Homepage': {
        zh: '返回首页',
        en: 'Homepage'
    },
    'Profile': {
        zh: '资料',
        en: 'Profile'
    },
    'Reservations': {
        zh: '订单',
        en: 'Reservations'
    },
    'Credits': {
        zh: '积分',
        en: 'Credits'
    },
    'Favorites': {
        zh: '收藏',
        en: 'Favorites'
    },
    'Concierge': {
        zh: '私人管家',
        en: 'Concierge'
    },
    'Concierge Services': {
        zh: '私人管家服务',
        en: 'Concierge Services'
    },
    'Anniversaries': {
        zh: '纪念日',
        en: 'Anniversaries'
    },
    'Logout': {
        zh: '注销',
        en: 'Logout'
    },
    'Login': {
        zh: '登录',
        en: 'Login'
    },
    'Register': {
        zh: '注册',
        en: 'Register'
    },
    'Social Login': {
        zh: '公共帐号登录',
        en: 'Social Login'
    },
    'Email': {
        zh: '邮箱',
        en: 'Email'
    },
    'Mobile': {
        zh: '手机',
        en: 'Mobile'
    },
    'Mobile/Email': {
        zh: '手机/邮箱',
        en: 'Mobile/Email'
    },
    'Birthday': {
        zh: '生日',
        en: 'Birthday'
    },
    'Save': {
        zh: '保存',
        en: 'Save'
    },
    'Name': {
        zh: '姓名',
        en: 'Name'
    },
    'Modify Profile': {
        zh: '修改资料',
        en: 'Modify Profile'
    },
    'Modify Password': {
        zh: '修改密码',
        en: 'Modify Password'
    },
    'Confirm Password': {
        zh: '确认密码',
        en: 'Confirm Password'
    },
    'Password': {
        zh: '密码',
        en: 'Password'
    },
    'Contact': {
        zh: '联系人',
        en: 'Contact'
    },
    'Contacts': {
        zh: '联系人',
        en: 'Contacts'
    },
    'Online Contacts': {
        zh: '美位联系人',
        en: 'Online Contacts'
    },
    'Local Contacts': {
        zh: '手机通讯录',
        en: 'Local Contacts'
    },
    'Gifts': {
        zh: '礼品',
        en: 'Gifts'
    },
    'Anniversary': {
        zh: '纪念日',
        en: 'Anniversary'
    },
    'Add': {
        zh: '添加',
        en: 'Add'
    },
    'Title': {
        zh: '标题',
        en: 'Title'
    },
    'Date': {
        zh: '日期',
        en: 'Date'
    },
    'Time': {
        zh: '时间',
        en: 'Time'
    },
    'Delete': {
        zh: '删除',
        en: 'Delete'
    },
    'New Orders': {
        zh: '新订单',
        en: 'New Orders'
    },
    'Pending Orders': {
        zh: '未完成订单',
        en: 'Pending Orders'
    },
    'Fulfilled Orders': {
        zh: '已完成订单',
        en: 'Fulfilled Orders'
    },
    'Detail': {
        zh: '详情',
        en: 'Detail'
    },
    'Checkin': {
        zh: '签到 &amp; 分享',
        en: 'Checkin'
    },
    'Settings': {
        zh: '设置',
        en: 'Settings'
    },
    'Language': {
        zh: '语言',
        en: 'Language'
    },
    'Search Hint': {
        zh: '搜索餐厅',
        en: 'Search Restaurants'
    },
    'No Orders! Go and Book!': {
        zh: '还没有订单，赶紧预订！',
        en: 'No Orders! Go and Book!'
    },
    '两次密码输入不一致，请重新输入。': {
        zh: '两次密码输入不一致，请重新输入。',
        en: '两次密码输入不一致，请重新输入。'
    },
    'Please confirm the cancellation': {
        zh: '请确认删除订单',
        en: 'Cancel Order'
    },
    'An SMS will be sent to you to inform you the order has been confirmed': {
        zh: '订单被确认以后您会收到一条短信',
        en: 'An SMS will be sent to you to inform you the order has been confirmed'
    },
    'We will contact you as soon as possible,thanks for order our service': {
        zh: '美位网客服将尽快与您联系，和您进一步沟通服务预订的需求。',
        en: 'We will contact you as soon as possible,thanks for order our service'
    },
    'Cancel Order': {
        zh: '删除订单',
        en: 'Cancel Order'
    },
    'Confirm Order': {
        zh: '确认订单',
        en: 'Confirm Order'
    },
    'Confirm': {
        zh: '确认',
        en: 'Confirm'
    },
    'Waiting Confirm': {
        zh: '等待确认',
        en: 'Waiting Confirm'
    },
    'Gender': {
        zh: '称呼',
        en: 'Gender'
    },
    'Pay': {
        zh: '支付',
        en: 'Pay'
    },
    'Cancel': {
        zh: '取消',
        en: 'Cancel'
    },
    'Close': {
        zh: '关闭',
        en: 'Close'
    },
    'Yes': {
        zh: '是',
        en: 'Yes'
    },
    'No': {
        zh: '否',
        en: 'No'
    },
    'Order No': {
        zh: '订单号',
        en: 'Order No'
    },
    'Order Date': {
        zh: '订单日期',
        en: 'Order Date'
    },
    'Order Time': {
        zh: '订单时间',
        en: 'Time'
    },
    'People': {
        zh: '人数',
        en: 'People'
    },
    'Discount': {
        zh: '折扣',
        en: 'Discount'
    },
    'Address': {
        zh: '地址',
        en: 'Address'
    },
    'Working Hour': {
        zh: '工作时间',
        en: 'Working Hour'
    },
    'Amount': {
        zh: '消费金额',
        en: 'Amount'
    },
    'Confirm Amount': {
        zh: '确认金额',
        en: 'Confirm Amount'
    },
    'Share to Moments': {
        zh: '分享到朋友圈',
        en: 'Share to Moments'
    },
    'Share to Weibo': {
        zh: '分享到微博',
        en: 'Share to Weibo'
    },
    'Purchase': {
        zh: '兑换',
        en: 'Purchase'
    },
    'credits left. One of our customer service will contact you soon as possible.': {
        zh: '剩余积分。我们的客服会马上联系您。',
        en: ' credits left. One of our customer service will contact you soon as possible.'
    },
    'Successfully! You have': {
        zh: '兑换成功！您还有',
        en: 'Successfully! You have '
    },
    'Share Meiwei with your friends': {
        zh: '和朋友们分享美位',
        en: 'Share Meiwei with your friends'
    },
    'You will receive a gift after sharing': {
        zh: '分享后可以得到精美礼品一份',
        en: 'You will receive a gift after sharing'
    },
    'Airport Arrival Time': {
        zh: '降落时间',
        en: 'Airport Arrival Time'
    },
    'Airport Departure Time': {
        zh: '起飞时间',
        en: 'Airport Departure Time'
    },
    'Airport Arrival': {
        zh: '到达机场',
        en: 'Airport Arrival'
    },
    'Airport Departure': {
        zh: '出发机场',
        en: 'Airport Departure'
    },
    'Comment': {
        zh: '备注',
        en: 'Comment'
    },
    'My Cakes': {
        zh: '我的蛋糕',
        en: 'My Cakes'
    },
    'Refine Your Search': {
        zh: '分类查找',
        en: 'Refine Your Search'
    },
    'Reset Search': {
        zh: '重置搜索',
        en: 'Reset Search'
    },
    'Top List': {
        zh: '美位榜单',
        en: 'Top List'
    },
    'Popular Circles': {
        zh: '热门商圈',
        en: 'Popular Circles'
    },
    'Popular Cuisines': {
        zh: '热门菜系',
        en: 'Popular Cuisines'
    },
    'View Orders': {
        zh: '查看订单',
        en: 'View Orders'
    },
    'Order Now': {
        zh: '立即预定',
        en: 'Order Now'
    },
    'Size': {
        zh: '尺寸',
        en: 'Size'
    },
    'Theme': {
        zh: '主题',
        en: 'Theme'
    },
    'Cakes': {
        zh: '蛋糕',
        en: 'Cakes'
    },
    'Search': {
        zh: '搜索',
        en: 'Search'
    },
    'Browse': {
        zh: '浏览',
        en: 'Browse'
    },
    'Price': {
        zh: '人均',
        en: 'Price'
    },
    'Join Us': {
        zh: '加入美位',
        en: 'Join Us'
    },
    'Already Have an Account?': {
        zh: '已经注册？点击登录。',
        en: 'Already Have an Account?'
    },
    'Or Create Your Account': {
        zh: '还没注册？点击注册。',
        en: 'Or Create Your Account'
    },
    'Flight No.': {
        zh: '航班号',
        en: 'Flight No.'
    },
    'Contact Information': {
        zh: '联系方式',
        en: 'Contact Information'
    },
    'Car No.': {
        zh: '车牌号',
        en: 'Car No.'
    },
    'Add VIP': {
        zh: '添加贵宾',
        en: 'Add VIP'
    },
    'PVG': {zh: '上海浦东国际机场', en: 'Shanghai Pudong Airport'},
    'SHA': {zh: '上海虹桥国际机场', en: 'Shanghai Hongqiao Airport'},
    'PEK': {zh: '北京首都国际机场', en: 'Beijing Shoudu Airport'},
    'CAN': {zh: '广州白云国际机场', en: 'Guangzhou Baiyun Airport'},
    'CTU': {zh: '成都双流国际机场', en: 'Chengdu Shuangliu Airport'},
    'HGH': {zh: '杭州萧山国际机场', en: 'Hangzhou Xiaoshan Airport'},
    'CSX': {zh: '长沙黄花国际机场', en: 'Changsha Huanghua Airport'},
    'NGB': {zh: '宁波栎社国际机场', en: 'NingBo Lishe Airport'},
    'KHN': {zh: '南昌昌北国际机场', en: 'Nanchang Changbei Airport'},
    'KMG': {zh: '昆明长水国际机场', en: 'Kunming Changshui Airport'},
    'NKG': {zh: '南京禄口国际机场', en: 'Nanjing Lukou Airport'},
    'SYX': {zh: '三亚凤凰国际机场', en: 'Sanya Fenghuang Airport'},
    'SZX': {zh: '深圳宝安国际机场', en: 'Shenzhen Baoan Airport'},
    'XIY': {zh: '西安咸阳国际机场', en: 'Xian Xianyang Airport'},
    'SHE': {zh: '沈阳桃仙国际机场', en: 'Shenyang Taoxian Airport'},
    'TAO': {zh: '青岛流亭国际机场', en: 'Qingdao Liuting Airport'},
    'WUX': {zh: '无锡苏南国际机场', en: 'Wuxi Sunan Airport'},
    'HAK': {zh: '海口美兰国际机场', en: 'Haikou Meilan Airport'},
    'NNG': {zh: '南宁吴圩国际机场', en: 'Nanning Wuxu Airport'},
    'TSN': {zh: '天津滨海国际机场', en: 'Tianjin Binghai Airport'},
    'XMN': {zh: '厦门高崎国际机场', en: 'XiaMen Gaoqi Airport'},
    'CKG': {zh: '重庆江北国际机场', en: 'Chongqin Jiangbei Airport'},
    'FOC': {zh: '福州长乐国际机场', en: 'Fuzhou Changle Airport'},
    'WUH': {zh: '武汉天河国际机场', en: 'Wuhan Tianhe Airport'},
    'DYG': {zh: '张家界荷花机场', en: 'Zhangjiajie Hehua Airport'},
    'YNT': {zh: '烟台莱山机场', en: 'Yantai Laishan Airport'},
    'URC': {zh: '乌鲁木齐地窝堡国际机场', en: 'Wulumuqi Diwobao Airport'},
    'KWE': {zh: '贵阳龙洞堡国际机场', en: 'Guiyang Longdongbao Airport'},
    'WNZ': {zh: '温州龙湾国际机场', en: 'Wenzhou Longwan Airport'},
    'LJG': {zh: '丽江机场', en: 'Lijiang Airport'},
    'KWL': {zh: '桂林两江机场', en: 'Guilin Liangjiang Airport'},
    'JHG': {zh: '西双版纳机场', en: 'Xishuangbanna Airport'},
    'TYN': {zh: '太原武宿机场', en: 'Taiyuan Wusu Airport'},
    'HET': {zh: '呼和浩特白塔国际机场', en: 'Huhehaote Baita Airport'},
    'HRB': {zh: '哈尔滨太平国际机场', en: 'Harbin Taiping Airport'},
    'INC': {zh: '银川河东机场', en: 'Yinchuan Hedong Airport'},
    'JJN': {zh: '泉州晋江机场', en: 'Quanzhou Jinjiang Airport'},
    'DSN': {zh: '鄂尔多斯机场', en: 'Erduosi Airport'},
    'CZX': {zh: '常州奔牛机场', en: 'Changzhou Benniu Airport'},
    'XUZ': {zh: '徐州观音机场', en: 'Xuzhou Guanyin Airport'},
    'MIG': {zh: '绵阳南郊机场', en: 'Mianyang Nanjiao Airport'},
    'KOW': {zh: '赣州黄金机场', en: 'Ganzhou Huangjin Airport'},
    'HKG': {zh: '香港国际机场', en: 'Hongkong Airport'},
    'MFM': {zh: '澳门国际机场', en: 'Macau Airport'},
    'Pick-up Location': {
        zh: '接送地点',
        en: 'Pick-up Location'
    },
    'Accompanies': {
        zh: '陪同人数',
        en: 'Accompanies'
    },
    'Services Type': {
        zh: '服务类型',
        en: 'Services Type'
    },
    'Arrival Service': {
        zh: '入港服务',
        en: 'Arrival Service'
    },
    'Departure Service': {
        zh: '出港服务',
        en: 'Departure Service'
    },
    'Guest Information': {
        zh: '贵宾信息',
        en: 'Guest Information'
    },
    'Airport Services' :{
        zh:'机场服务',
        en:'Airport Services'
    },
    'Confirmed Orders':{
        zh:'已确认订单',
        en:'Confirmed Orders'
    },
    'Payed Amount': {
        zh: '支付金额',
        en: 'Payed Amount'
    },
    'My Orders':{
        zh:'我的订购',
        en:'My Orders'
    },
    'Please confirm your order':{
        zh:'请确认您的订单',
        en:'Please confirm your order'
    },
    'By clicking Confirm, you agree and that you have read our Services Policy':{
        zh:'点击确认即表明你同意且你已阅读过我们的',
        en:'By clicking Confirm, you agree and that you have read our Services Policy'
    },
    'Services Policy':{
        zh: '服务条款',
        en: 'Services Policy'
    },
    'Successfully Confirmed Order': {
        zh: '订单确认成功',
        en: 'Successfully Confirmed Order'
    },
    'Successfully Completed Order': {
        zh: '订单完成成功',
        en: 'Successfully Completed Order'
    },
    'Cancelled Order': {
        zh: '已取消订单',
        en: 'Cancelled Order'
    },
    'Confirmed Order': {
        zh: '已确认订单',
        en: 'Confirmed Order'
    },
    'Coupon No.':{
        zh: '优惠券号码',
        en: 'Coupon No.'
    },
    'Coupon Confirmed Success':{
        zh: '优惠券确认成功',
        en: 'Coupon Confirmed Success'
    },
    'Password Update Success':{
        zh: '密码修改成功',
        en: 'Password Change Success'
    },
    'Profile Update Success':{
        zh: '资料修改成功',
        en: 'Profile Change Success'
    }
};

MeiweiApp.CheckI18n = function () {
    var newMsg = [];
    var checkExist = function (el) {
        var msg = $(el).attr('data-i18n');
        if (msg && !MeiweiApp.i18n[msg])
            newMsg.push(msg);
        msg = $(el).attr('data-placeholder-i18n');
        if (msg && !MeiweiApp.i18n[msg])
            newMsg.push(msg);
    }
    $('[data-i18n], [data-placeholder-i18n]').each(function () {
        checkExist(this);
    });
    for (var i in TPL) {
        var template = TPL[i]();
        $(template).find('[data-i18n], [data-i18n-placeholder]').each(function () {
            checkExist(this);
        });
        $(template).filter('[data-i18n], [data-i18n-placeholder]').each(function () {
            checkExist(this);
        });
    }
    return newMsg;
};
