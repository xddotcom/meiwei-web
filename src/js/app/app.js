MeiweiApp = new (Backbone.View.extend({
    Models: {},
    Views: {},
    Collections: {},

    Templates: {},
    Pages: {},

    configs: {
        APIHost: "/api/meiwei",
        StaticURL: "/assets/",
        ajaxTimeout: 10000
    },

    start: function () {
        MeiweiApp.initDevice();
        MeiweiApp.initAjaxEvents();
        MeiweiApp.initLang();
        MeiweiApp.initSync();
        //Backbone.history.start();
    }
}))({el: document.body});

MeiweiApp.initDevice = function () {
    var userAgent = navigator.userAgent;
    MeiweiApp.isMobile = /Mobile/i.test(userAgent);
    var isIe = /compatible/i.test(userAgent);
    var isWebkit = /webkit/i.test(userAgent);
    if(isIe){
        if(/MSIE 8.0/i.test(userAgent)){
            Backbone.emulateHTTP = true;
            MeiweiApp.IE8_Compatible = true;
        }
    }
    if(!isWebkit){
        $('select.form-control').css('background','none').css('padding-right',10);
    }
};

MeiweiApp.initLang = function () {
    var langCode = localStorage.getItem('lang-code') || 'zh';
    MeiweiApp._ = function (msgId) {
        var msg = MeiweiApp.i18n[msgId];
        return msg ? msg[langCode] : msgId;
    };
    MeiweiApp.initLang = function (context) {
        context = context || document;
        $(context).find('[data-i18n]').each(function () {
            $(this).html(MeiweiApp._($(this).attr('data-i18n')));
        });
        $(context).find('[data-placeholder-i18n]').each(function () {
            $(this).attr('placeholder', MeiweiApp._($(this).attr('data-placeholder-i18n')));
        });
    };
    MeiweiApp.setCookieLang = function (lang) {
        var Days = 30000;
        var exp = new Date();
        exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
        document.cookie = 'lang-code' + "=" + escape(lang) + ";path=/;expires=" + exp.toGMTString();
    };
    MeiweiApp.setLang = function (lang) {
        localStorage.setItem('lang-code', (langCode = lang));
        MeiweiApp.setCookieLang(lang);
        MeiweiApp.initLang();
    };
    MeiweiApp.getLang = function () {
        return langCode;
    };
    MeiweiApp.initLang();
};

MeiweiApp.initSync = function () {
    var authToken = localStorage.getItem('auth-token');
    var originalSync = Backbone.sync;
    Backbone.sync = function (method, model, options) {
        options.timeout = options.timeout || MeiweiApp.configs.ajaxTimeout;
        _.extend((options.headers || (options.headers = {})), { 'Accept-Language': MeiweiApp.getLang() });
        if (authToken) {
            _.extend(options.headers, { 'Authorization': 'Token ' + authToken });
        }
        if (options.nocache) {
            _.extend(options.headers, { 'Cache-Control': 'no-cache' });
        }
        if (options.url) {
            options.url = options.url.replace(/^(?:http|https)\:\/{2}[a-zA-Z0-9\-_\.]+(?:\:[0-9]{1,4})?(.*)/,
                MeiweiApp.configs.APIHost + '$1');
        }
        return originalSync.call(model, method, model, options);
    };
    MeiweiApp.TokenAuth = {
        get: function () {
            return _.clone(authToken);
        },
        set: function (token) {
            authToken = _.clone(token);
            localStorage.setItem('auth-token', authToken);
        },
        clear: function () {
            authToken = null;
            localStorage.removeItem('auth-token');
        }
    };
};

MeiweiApp.initAjaxEvents = function () {
    $(document).ajaxError(function (event, jqxhr, settings, exception) {
        var response = jqxhr.responseJSON || {};
        if (jqxhr.status == 401 || jqxhr.status == 403 || jqxhr.status == 499) {
            MeiweiApp.TokenAuth.clear(); //TODO: Essential! Otherwise a wrong token maybe submit along with login params.
            MeiweiApp.Pages.MemberLoginPopup.go();
        }
    });
};

MeiweiApp.handleError = function (err) {
    try {
        var error = new MeiweiApp.Models.ClientError();
        error.save({message: 'web: ' + err.message, detail: err.stack}, {global: false});
        console.error(err.message);
    } catch (e) {
    }
};
