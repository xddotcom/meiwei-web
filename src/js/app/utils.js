MeiweiApp.displayAlert = function (info) {
    var $alert = $('.global-info-prompt-box');
    $alert.find('.info-prompt').html(info);
    $alert.addClass('show');
    if (MeiweiApp.promptTimer) {
        clearTimeout(MeiweiApp.promptTimer);
    }
    MeiweiApp.promptTimer = setTimeout(function () {
        $alert.removeClass('show');
    }, 3000);
    $alert.find('.prompt-close').one('click', function () {
        $alert.removeClass('show');
    });
};

MeiweiApp.showConfirmDialog = function (content, options) {
    options = options||{};
    var dialog = new (MeiweiApp.View.extend({
        template: TPL['confirm-dialog'],
        events: {
            'click .btn-cancel': 'onCancelClick',
            'click .btn-confirm': 'onConfirmClick',
            'hidden.bs.modal': 'closeDialog'
        },
        initView: function () {
            _.bindAll(this, 'closeDialog', 'openDialog', 'onConfirmClick', 'render', 'onCancelClick');
        },
        onCancelClick: function () {
            this.$('#confirm-dialog').modal('hide');
            if (options.onCancel) {
                options.onCancel();
            }
        },
        closeDialog: function () {
            this.remove();
            this.undelegateEvents();
        },
        openDialog: function () {
            $('body').append(this.el);
            var options = {
                show: true,
                backdrop: true
            };
            this.$('#confirm-dialog').modal(options);
            this.delegateEvents();
        },
        onConfirmClick: function () {
            this.$('#confirm-dialog').modal('hide');
            if (options.onConfirm) {
                options.onConfirm();
            }
        },
        render: function () {
            this.renderTemplate({title: options.title, content: content});
            this.openDialog();
            return this;
        }
    }))();
    dialog.remove();
    dialog.render();
};
