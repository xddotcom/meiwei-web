var log4js = require('log4js');
var CONFIG = require('../settings/settings');
log4js.configure({
    appenders: [
        {
            type: 'file',
            filename: __dirname + '/../log/cheese.log',
            category: 'meiwei'
        }
    ]
});
var logger = log4js.getLogger('meiwei');
logger.setLevel(CONFIG.LOGGER_LEVEL);
logger.addListener('log', function (e) {
    var levelStr = e && e.level && e.level.levelStr;
    if (levelStr == 'ERROR') {
        console.log('ERROR');
    }
});
module.exports = logger;


//module.exports = {
//    ALL: new Level(Number.MIN_VALUE, "ALL"),
//    TRACE: new Level(5000, "TRACE"),
//    DEBUG: new Level(10000, "DEBUG"),
//    INFO: new Level(20000, "INFO"),
//    WARN: new Level(30000, "WARN"),
//    ERROR: new Level(40000, "ERROR"),
//    FATAL: new Level(50000, "FATAL"),
//    OFF: new Level(Number.MAX_VALUE, "OFF"),
//    toLevel: toLevel
//};

//var logEvent = { startTime: 'Thu Jan 09 2014 11:54:36 GMT+0800 (中国标准时间)',
//    categoryName: 'meiwei',
//    data: [ 'server start listen port:8081' ],
//    level: { level: 20000, levelStr: 'INFO' },
//    logger: { category: 'meiwei',
//        _events: { log: [Object] },
//        level: { level: 20000, levelStr: 'INFO' } } }