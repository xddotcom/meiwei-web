/*!
 * connect-mongo
 * Copyright(c) 2011 Casey Banner <kcbanner@gmail.com>
 * MIT Licensed
 */

/**
 * Module dependencies
 */

var mongo = require('mongodb');
var CONFIG = require('../settings/settings');

var defaultOptions = {host: '127.0.0.1',
    port: 27017,
    stringify: true,
    collection: 'sessions',
    auto_reconnect: true,
    ssl: false,
    w: 1};

var options = {
    collection:'sessions',
    db:CONFIG.MONGO_DB,
    host:CONFIG.MONGO_SERVER,
    username:CONFIG.MONGO_USER,
    password:CONFIG.MONGO_PASSWORD
};

function MongoConnection(options){

    var self = this;

    this.db = new mongo.Db(options.db,
        new mongo.Server(options.host || defaultOptions.host,
            options.port || defaultOptions.port,
            {
                auto_reconnect: options.auto_reconnect ||
                    defaultOptions.auto_reconnect,
                ssl: options.ssl || defaultOptions.ssl
            }),
        { w: options.w || defaultOptions.w });

    self.collections = new Array();

    self.db.open(function (err, db) {

        if (err) {
            console.log(err);
            return;
        }

        self.db = db;

        if (options.username && options.password) {
            self.db.authenticate(options.username, options.password, function (err,result) {
                if(err){
                    console.log(err);
                }
            })
        }
    });

    this._get_collection = function (collectionName,callback) {
        if(!self.db){
            callback(new Error('mongodb database is null'));
            return;
        }
        if (self.collections[collectionName]) {
            callback && callback(null,self.collections[collectionName]);
        } else {
            self.db.collection(collectionName, function (err, collection) {
                if (err) {
                    var err = new Error('Error getting collection: ' + self.db_collection_name + ' <' + err + '>');
                    callback(err);
                    return;
                } else {
                    self.collections[collectionName] = collection;
                    callback && callback(null,collection);
                }
            });
        }
    };
}

module.exports = new MongoConnection(options);





