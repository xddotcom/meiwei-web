var http = require('http');
var qs = require('querystring');
var crypto = require('crypto');
var CONFIG = require('../settings/settings');

exports.cake = function(req, res) {
    res.redirect('/product/#cake');
    //res.locals.STATIC_CSS = ['views/product-cake.css'];
    //res.locals.STATIC_JS = ['views/product-cake.js'];
    //res.render('products/cake.html');
};

exports.productList = function(req, res) {
    res.locals.STATIC_CSS = ['views/product-list.css'];
    res.locals.STATIC_JS = ['views/product-list.js'];
    res.render('products/product-list.html');
};

exports.productDetail = function(req, res) {
    var data = {
        product_id : req.params.product_id
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/order-product.css'];
    res.locals.STATIC_JS = ['views/order-product.js'];
    res.render('products/order-product.html');
};
