/**
 * Created by weixumian on 13-10-31.
 */

var WECHAT_POST = {
    'NEW_RESTAURANT_0': {
        title: '瑠RYU',
        description: '外滩5号新入驻的瑠RYU，以优雅浪漫的法式日料为上海的饕客带来最新美食去处。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59ftVHLAo2fevF3EPAanlHqYqiau6gxiaFeTMwrlze6K1pk9Bwl3JsIAp8A/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219209&idx=1&sn=407a887e99c12ab7c96ddd3667a2f5c9#rd'
    },
    'NEW_RESTAURANT_1': {
        title: '茹丝葵牛排馆',
        description: "1965年在新奥尔良建立的茹丝葵牛排馆（Ruth’s Chris Steak House)一直以顶级牛排闻名。",
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59f8f2ha1tiapCVmQetpXGkWGHTuDlQ730OqUretWXNEUMcJs27xACTn2A/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219209&idx=2&sn=8f648a70ed68ac8585d88946d6dab2be#rd'
    },
    'NEW_RESTAURANT_2': {
        title: 'Chef Alex',
        description: '自法国著名糕点学院INBP学成归国的帅哥老板；食材全法国进口的诱人甜品，这就是人气火爆的甜品店Chef Alexander。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59f1bNHx2ylicUA3YNevIBUm6KcicvMUjCOusuqNiaGVS0S2ajNjAh6byC3Q/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219209&idx=3&sn=8bf21f470899a6f09a2daa498a181c67#rd'
    },
    'NEW_RESTAURANT_3': {
        title: '囍娜湘香',
        description: '在一家家各式西餐林立的新天地区域，“囍娜湘香”可谓独树一帜，鲜香麻辣的湘菜吸引了不少嗜辣的老饕，来品尝这独特的摩登湘菜。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fMMGCibKo5qwzxqlFt1JX0HlYY0iaV4CSzPBpH4SB0NPtZGXZWp71R2Pg/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219209&idx=4&sn=c6cd0d90e61114e465e13a847540a536&scene=1#rd'
    },
    'NEW_PRODUCT_0': {
        title: 'TED BEAR',
        description: '洋娃娃的手里总有小熊，我的心上人有一个会说话的熊。希望TED充盈了你心里的童趣，永远都是我的小女孩。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59f1PgicF3L5fHSq5Mjas3stgx0EzTThKq4hboymTxQ4qcskJCkTkpn1nA/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219690&idx=1&sn=1e9d832d37a6d95e5e869515b8d33735#rd'
    },
    'NEW_PRODUCT_1': {
        title: '薰衣草永生花',
        description: '“等待爱情”薰衣草永生花花盒－清雅隽永，正如我们的爱情，不随时间流逝而失色，浪漫的与众不同。让我们的爱情永生。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fCP2CS9TfMwNCZufeQrjKVne3nuSpmVEGJEUWwJialgibViawic0kqKd46g/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219690&idx=2&sn=cb447df08c9d8fcf81a005bc8ac73cd3#rd'
    },
    'NEW_PRODUCT_2': {
        title: '永生花盒',
        description: "梦幻色彩的鲜花花盒，满满一大盒的饱满浓情，让你拥有满怀的鲜花和幸福",
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fusy1UMS2CTYPkQeGibEQ03NUIayRqWdJRwaHufrkYfkExY0tlE7WkPw/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219690&idx=3&sn=aa03aed7e822724ed95fd13dc71eaaf8#rd'
    },
    'NEW_PRODUCT_3': {
        title: '翻糖蛋糕',
        description: '延展性极佳的翻糖，天生具备的丰富色彩，也可以塑造出各式各样的造型，用您的灵巧心思，和我们一起携手完成杰作。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59f2xV5YEfApvhrMrZ77XWjxxSjQoUPZG0WpiaWr112fibDBC1l0CzOAEgg/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219690&idx=4&sn=e8bd307f8e84dbe316d961ccf72b58bf#rd'
    },
    'NEW_PRODUCT_4': {
        title: '马卡龙塔',
        description: '漂亮的马卡龙就如珠宝班的陈列！美得令您屏息，让您仿佛置身香榭丽舍大道的美味',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fG7CBeLC7V9plYCMSu2NeQtFGwHyPibLttRJSKokPFFPStPCEJ8zfyhw/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219690&idx=5&sn=3c8ef4e62b90e45ed3506e722ab2ffd7#rd'
    },
    'NEW_EVENT_0': {
        title: '橄榄餐厅评论·鲜的中国味 美食大赏',
        description: '',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fPNvssx6Ijr2YFNShQF7GotjHS0Rmvp9Lus6QHr0wvgibtQdR69omMAg/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219775&idx=1&sn=b55f7380ddda7a40b0fd62e6c6f56359#rd'
    },
    'NEW_EVENT_1': {
        title: '2014海天盛筵',
        description: '海天盛筵China Rendez-Vous，是一场由鸿洲集团发起并创办的汇集全球公务机、游艇及配套产品、高级轿跑车、珠宝精品、生活艺术品等世界顶尖品牌于一身的高尚生活方式展。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fdJAvG65n8PM3J3s5qRVHpQjD5CJ1rVicdm9R6XM54Oiaia5ZzkADzGoog/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219775&idx=2&sn=1622856165afab70fcde459a729d783a#rd'
    },
    'JIERIFANGANCEHUA': {
        title: '节日方案策划',
        description: '自己的生日，TA的生日，父母、亲朋、好友的生日……相识纪念、交往纪念、结婚纪念……越来越多的纪念日，难免令人有厌倦和雷同的感觉。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fDgrDEPG3ia7NnHKPvIKvSEKszlfvgs4gFX4wfSq7r5O8gpjibI7elPrA/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219942&idx=1&sn=b7965f5575cc4e80e47a1c539ae13873#rd'
    },
    'CEHUAZHIXING': {
        title: '策划执行',
        description: '求婚、婚宴、生日宴、年会、商务活动、私人派对……流程策划、场地布置全程跟踪',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59faHLick2P5Im2K1RxBaJ2OlBkV5tybxevIuz9xia2icZedXw22zEwzoWXA/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200220089&idx=3&sn=e1efc787ed4aa4ec971f4b67e7607c5c#rd'
    },
    'CHANGDITUIJIAN': {
        title: '场地推荐',
        description: '沪上300多家中高档餐饮品牌场地资源，覆盖上海所有地区，满足各类需求，餐厅选址、环境、口味、人均…… 为您的用餐提供最贴心的全套搭配',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59faDT6Qasf4BgeicM2z0H4u9p2ry7N9BLGpL9E0IPZ5icAQ0CcdjnuMtjw/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200220089&idx=1&sn=857d24f31913885eaba9b863c2cf10ae#rd'
    },
    'GAODUANSHANGLV': {
        title: '高端商旅',
        description: '私人包机、顶级酒店、环游世界、海岛度假、遍尝米其林美食……美位为您打造每一趟顶级商旅行程，尽情享受无拘束的奢华之旅，敬请期待！',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fbHsAYH6F6AcFiahG5SKUs80bLVSdH1aNibaLhxsjtc0IZ5MPcucXU3ibg/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200219942&idx=2&sn=b975f64ce1f8df77c3e26dc8ffdced73#rd'
    },
    'CANYINGFUWU': {
        title: '餐饮服务',
        description: '五星级厨师团队到场全程外烩，品质一如在五星级酒店用餐般精致，却又能同时享有家族聚餐才有的浓浓的人情味。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fh03AibMEBJeLSMbb0feP7D0Y6pbHXcfvFqJnYC8cJT2pHh4uibZrMWxw/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200220089&idx=2&sn=26a19e34e5e75be8d2a4c3954af663ea#rd'
    },
    'HUIWUZHIXING': {
        title: '会务执行',
        description: '覆盖沪上所有地区，拥有丰富的各类场地资源，满足企业各类会务、公关活动等场地需求',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fotYjWkmMXxhqpyJGJFKlWEFqlmp2l0LZsxXKUH5f3HiaSASzMSbgaIw/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200220089&idx=4&sn=727466326222b10e94ea781ac39be81a#rd'
    },
    'QIYEDIAOYAN': {
        title: '企业调研',
        description: '美位拥有沪上各领域大中型企业用户资源，不仅为您的企业调研提供推荐与咨询服务，还可为您提供相关会面、沟通等事宜，为您的调研工作提供全程陪同与支持。',
        picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fRIj37Xj8IG3thYdokKuGuRoicFn5sIiaJoWXrVFLWQ2qYLnZIyzztYNg/0',
        url: 'http://mp.weixin.qq.com/s?__biz=MjM5NTQyNTgyMQ==&mid=200220089&idx=5&sn=52a7a708e081d484f6f8c8e6806859e7#rd'
    }
};

module.exports = WECHAT_POST;
