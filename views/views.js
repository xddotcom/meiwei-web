var http = require('http');
var qs = require('querystring');
var CONFIG = require('../settings/settings');

exports.home = function(req, res, next) {
    res.locals.STATIC_CSS = ['views/home.css'];
    res.locals.STATIC_JS = ['views/home.js'];
    res.render('home');
};

exports.newHome = function(req, res, next) {
    res.locals.STATIC_CSS = ['views/new-home.css'];
    res.locals.STATIC_JS = ['views/new-home.js'];
    res.render('new-home');
};

exports.restaurantSearch = function(req, res, next) {
    var data = {
        keywords : req.query.q,
        recommend : req.query.r
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/restaurant-search.css'];
    res.locals.STATIC_JS = ['views/restaurant-search.js'];
    res.render('restaurant/search.html');
};

exports.restaurantDetail = function(req, res, next) {
    var data = {
        restaurantId : parseInt(req.params.restaurant_id)
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/restaurant-detail.css'];
    res.locals.STATIC_JS = ['views/restaurant-detail.js'];
    res.render('restaurant/detail.html');
};

exports.restaurantOrder = function(req, res, next) {
    var data = {
        restaurantId : parseInt(req.params.restaurant_id),
        order : {
            personnum : req.query.personnum,
            ordertime : req.query.ordertime,
            orderdate : req.query.orderdate || new Date().toISOString().substring(0, 10)
        }
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/restaurant-order.css'];
    res.locals.STATIC_JS = ['views/restaurant-order.js'];
    res.render('restaurant/order.html');
};

exports.restaurantPictures = function(req, res, next) {
    var data = {
        restaurantId : parseInt(req.params.restaurant_id)
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/restaurant-pictures.css'];
    res.locals.STATIC_JS = ['views/restaurant-pictures.js'];
    res.render('restaurant/pictures.html');
};

exports.memberLogin = function(req, res) {
    res.locals.SERVER_DATA = JSON.stringify({
        status : 'signin'
    });
    res.locals.STATIC_CSS = ['views/member-login.css'];
    res.locals.STATIC_JS = ['views/member-login.js'];
    res.render('member/login.html');
};

exports.memberRegister = function(req, res) {
    res.locals.SERVER_DATA = JSON.stringify({
        status : 'signup'
    });
    res.locals.STATIC_CSS = ['views/member-login.css'];
    res.locals.STATIC_JS = ['views/member-login.js'];
    res.render('member/login.html');
};

exports.memberDashboard = function(req, res) {
    res.locals.STATIC_CSS = ['views/dashboard.css'];
    res.locals.STATIC_JS = ['views/member-dashboard.js'];
    res.render('member/dashboard.html');
};

exports.staffDashboard = function(req, res) {
    res.locals.STATIC_CSS = ['views/dashboard.css'];
    res.locals.STATIC_JS = ['views/staff-dashboard.js'];
    res.render('staff/dashboard.html');
};

exports.staffAdminDashboard = function(req, res) {
    res.locals.STATIC_CSS = ['views/dashboard.css'];
    res.locals.STATIC_JS = ['views/staff-admin-dashboard.js'];
    res.render('staff/admin-dashboard.html');
};

exports.apppromo = function(req, res, next) {
    var userAgent = req.headers['user-agent'];
    var isWeixin = userAgent.match(/MicroMessenger/i) == "MicroMessenger" ? true : false;
    if (!isWeixin) {
        if (/iPhone/i.test(userAgent)) {
            res.redirect(301, 'https://itunes.apple.com/app/id689668571');
            return;
        } else if (/android/i.test(userAgent)) {
            res.redirect(301, 'http://web.clubmeiwei.com/assets/MeiweiApp.apk');
            return;
        }
    }
    res.locals.is_weixin = isWeixin;
    res.locals.STATIC_CSS = ['views/simpleview.css'];
    res.render('ad/apppromo.html');
};

exports.gewara = function(req, res, next) {
    res.locals.STATIC_CSS = ['views/simpleview.css'];
    res.render('ad/gewara.html');
}

exports.staticPages = function(req, res) {
    res.locals.STATIC_CSS = ['views/simpleview.css'];
    res.render('aboutus/' + req.params.static_name + '.html');
};
