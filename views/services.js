var http = require('http');
var qs = require('querystring');
var crypto = require('crypto');
var CONFIG = require('../settings/settings');

exports.orderAirport = function(req, res) {
    res.locals.STATIC_CSS = ['views/order-airport.css'];
    res.locals.STATIC_JS = ['views/order-airport.js'];
    res.render('services/order-airport.html');
};

exports.orderPingAn = function(req, res) {
    var data = {
        coupon : req.query.coupon
    };
    res.locals.SERVER_DATA = JSON.stringify(data);
    res.locals.STATIC_CSS = ['views/order-pingan.css'];
    res.locals.STATIC_JS = ['views/order-pingan.js'];
    res.render('services/order-pingan.html');
};

exports.vvipDashboard = function(req, res) {
    res.locals.STATIC_CSS = ['views/dashboard.css'];
    res.locals.STATIC_JS = ['views/vvip-dashboard.js'];
    res.render('services/vvip-dashboard.html');
};

exports.couponValidate = function(req, res) {
    res.locals.STATIC_CSS = ['views/coupon-validate.css'];
    res.locals.STATIC_JS = ['views/coupon-validate.js'];
    res.render('services/coupon-validate.html');
};
