var events = require('events');
var emitter = new events.EventEmitter();
var http = require('http');
var https = require('https');

var mongo = require('../utils/mongo-connect');
var uuid = require('node-uuid');
var fs = require('fs');
var crypto = require('crypto');

var wechatPost = require('../views/wechat-post');

var CONFIG = require('../settings/settings');
var logger = require('../utils/logger.js');
var utils = require('../utils/utils.js');

function MeiweiWeixin(req, res, next) {
    this.appid = CONFIG.WEIXIN_APPID;
    this.appsecret = CONFIG.WEIXIN_APPSECRET;
    this.res = res;
    this.req = req;
    this.next = next;
    this.msg = req.weixin;
    logger.info(JSON.stringify(this.msg));
    this.openid = this.msg.FromUserName;
    this.public_id = this.msg.ToUserName;
    this.wxsession = req.wxsession;
    this.req.session.destroy();
}

MeiweiWeixin.prototype.getDefaultTextMsg = function () {
    var now = Math.round(new Date().getTime() / 1000);
    var flag = 0;
    if (this.msg.Event == 'subscribe') {
        flag = 1;
    }
    if (!flag) {
        if (!this.wxsession) {
            return null;
        }
        if (this.wxsession && this.wxsession.last_msg_time) {
            var lastMsgTime = Math.round(new Date(this.wxsession.last_msg_time) / 1000);
            if (lastMsgTime > (now - 24 * 60 * 60)) {
                return null;
            }
        }
    }

    var content = '';
    var user = this.user || {};
    content += (user.nickname || user.meiwei_nickname || '') +
        '您好!\nMeimei，Weiwei举手举爪欢迎您对美位网的关注！\n';
    if (!this.user.meiwei_token) {
        var link = CONFIG.SITE_URL + '/weixin/login?openid=' + this.openid + '&meiwei_token=' + (user.meiwei_token || '');
        content += '\n<a href="' + link + '">绑定美位账号</a>\n\n' + '获得更多美位服务. \n';
    }
    content += '发送#加关键字或者直接发送您的位置可以搜索餐厅。 \n';
    content += '特别提醒：每天晚上11点至次日早上9点，梦游中的Meimei和Weiwei回复较慢，请您谅解！';
    return content;

};

MeiweiWeixin.prototype.clickOnRestaurant = function (url, callback) {
    var self = this;
    var _data = '';
    var isBinding = false;
    var user = this.user || {};
    if (user.meiwei_token) {
        isBinding = true;
    }

    http.get(url,function (response) {
        if (response.statusCode >= 300) {
            var err = new Error("return not 200");
            callback && callback(err);
            return;
        }

        response.on('data',function (chunk) {
            _data += chunk;
        }).on('end', function () {
                var json;
                try {
                    json = JSON.parse(_data);
                } catch (e) {
                    var err = new Error('json parse error' + e.toString());
                    callback && callback(err);
                    return;
                }

                var result = json.results;
                //check is array
                if (Array.isArray(result)) {
                    var length = result.length;
                    if (length > 10) {
                        length = 10;
                    }
                    var articles = new Array();
                    for (var i = 0; i < length; i++) {
                        var article = {};
                        article.title = result[i].fullname;
                        article.description = result[i].description;
                        article.picUrl = result[i].frontpic;
                        if(isBinding){
                            article.url = CONFIG.MOBILE_SITE_URL + '/#regAuth/' + user.meiwei_token +'/restaurant/' + result[i].id;
                        } else {
                            article.url = CONFIG.MOBILE_SITE_URL + '/#restaurant/' + result[i].id;
                        }
                        articles.push(article);
                    }
                    if (articles.length) {
                        callback && callback(null, articles);
                        return;
                    }
                    callback && callback(new Error('get restaurant result is empty'));
                } else {
                    var err = new Error("json not array");
                    callback && callback(err);
                    return;
                }

            });
    }).on('error', function (e) {
            var err = new Error("get api from server network error:" + e.message);
            callback && callback(err);
            return;
        });
};

MeiweiWeixin.prototype.coordinateConvert = function (coordinate, callback) {
    var coordinate = coordinate || {
        lat: 31.23524,
        lng: 121.50582
    };
    var url = 'http://api.map.baidu.com/ag/coord/convert?from=0&to=4&x=' + coordinate.lng + '&y=' + coordinate.lat;
    http.get(url,function (response) {
        if (response.statusCode >= 300) {
            var errorMsg = "get map convert return not 200";
            logger.info(errorMsg);
            callback && callback(coordinate);
            return;
        }

        var _data = ''
        response.on('data',function (chunk) {
            _data += chunk;
        }).on('end', function () {
                var result;
                var isOk = true;
                try {
                    result = JSON.parse(_data);
                } catch (e) {
                    isOk = false;
                }

                if (result.error != 0) {
                    isOk = false;
                }

                if (isOk) {
                    coordinate.lng = parseFloat(new Buffer(result.x, 'base64').toString());
                    coordinate.lat = parseFloat(new Buffer(result.y, 'base64').toString());
                }

                callback && callback(coordinate);
            });

    }).on('error', function (e) {
            var errorMsg = "get api from get map convert network error:" + e.message;
            logger.info(errorMsg);
            callback && callback(coordinate);
        });
};

// 简单的缓存微信accessToken，accessToken每天限额是2000次，
// 每次两个小时的有效期，所以简单的将accessToken存储到内存中，减少调用
MeiweiWeixin.prototype.getWxAccessToken = function (callback) {
    var self = this;
    var now = Math.round(new Date().getTime() / 1000);

    mongo._get_collection('weixin_access_token', function (err, collection) {
        if (err) {
            callback && callback(err);
            return;
        }
        collection.findOne({_id: CONFIG.SERVICE_PUBLIC_NO}, function (err, weixinAccessToken) {
            if (err) {
                callback && callback(err);
                return;
            }
            if (weixinAccessToken) {
                if (((now - weixinAccessToken.create_time) - 3600) < 0) {
                    callback && callback(null, weixinAccessToken.access_token);
            return;
        }
    }

    var accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' +
        CONFIG.WEIXIN_APPID + '&secret=' + CONFIG.WEIXIN_APPSECRET;
    var _data = '';
    var accessToken = '';
    https.get(accessTokenUrl,function (response) {
        if (response.statusCode >= 300) {
            var errorMsg = "get access_token from weixin server return not 200";
            var err = new Error(errorMsg);
            callback(err);
            return;
        }

        response.on('data',function (chunk) {
            _data += chunk;
        }).on('end', function () {
                try {
                    var json = JSON.parse(_data);
                    if (json.errcode) {
                        var errorMsg = 'get access_token error:' + json.errmsg;
                        var err = new Error(errorMsg);
                        callback(err);
                        return;
                    } else {
                        accessToken = json.access_token;
                                var weixinAccessToken = {
                                    _id: CONFIG.SERVICE_PUBLIC_NO,
                                    access_token: json.access_token,
                                    create_time: Math.round(new Date().getTime() / 1000)
                                };

                                collection.update({
                                    _id: weixinAccessToken._id
                                }, weixinAccessToken, {
                                    upsert: true,
                                    safe: true
                                }, function (err, data) {
                                    if (err) {
                                        callback && callback(err);
                                        return;
                                    }
                        callback(null, accessToken);
                        return;
                                });
                    }
                } catch (e) {
                    var errorMsg = 'json parse error' + e.toString();
                    var err = new Error(errorMsg);
                    callback(err);
                    return;
                }
            });
    }).on('error', function (e) {
            var errorMsg = "delete access_token from weixin server network error:" + e.message;
            var err = new Error(errorMsg);
            callback(err);
            return;
        });

        });

    });
};

MeiweiWeixin.prototype.getWxUserInfo = function (callback) {
    var self = this;
    this.getWxAccessToken(function (err, accessToken) {
        if (err) {
            callback && callback(err);
            return;
        }

        var url = 'https://api.weixin.qq.com/cgi-bin/user/info?' + 'access_token=' + accessToken + '&openid=' + self.msg.FromUserName;

        https.get(url,function (response) {
            if (response.statusCode >= 300) {
                var errorMsg = "get userinfo from weixin server return not 200";
                var err = new Error(errorMsg);
                callback && callback(err);
                return;
            }

            var resData = ''
            response.on('data',function (chunk) {
                resData += chunk;
            }).on('end', function () {
                    var result;
                    try {
                        result = JSON.parse(resData);
                    } catch (e) {
                        var errorMsg = "get userinfo error:" + e.toString();
                        var err = new Error(errorMsg);
                        callback && callback(err);
                        return;
                    }
                    if (result.errcode) {
                        var errorMsg = "get userinfo error:" + result.errmsg;
                        var err = new Error(errorMsg);
                        callback && callback(err);
                        return;
                    }
                    callback && callback(null, result);
                })
        }).on('error', function (e) {
                var errorMsg = "get user info from weixin server network error:" + e.message;
                var err = new Error(errorMsg);
                callback && callback(err);
                return;
            });
    });
};

MeiweiWeixin.prototype.getOrSaveUser = function (callback) {
    var self = this;
    mongo._get_collection('wx_user', function (err, coll) {
        coll.findOne({_id: self.openid}, function (err, mgUser) {
            if (err) {
                logger.info(err.toString());
                callback && callback(err);
                return;
            }
            var flag = 0;
            if (mgUser) {
                flag = 1;
                if (mgUser.is_weixin_info) {
                    callback && callback(null, mgUser);
                    return;
                }
            }
            self.user = mgUser;
            self.getWxUserInfo(function (err, wxUser) {
                if (err) {
                    logger.info(err.toString());
                    if (flag) {
                        callback && callback(null, self.user);
                        return;
                    }
                }
                if (!wxUser) {
                    if (flag) {
                        callback && callback(null, self.user);
                        return;
                    }
                }
                if (flag) {
                    if (wxUser) {
                        self.user.is_weixin_info = 1;
                    }
                    utils.mergeJson(self.user, wxUser);
                    delete self.user._id;
                    coll.update({
                        _id: self.openid
                    }, {$set: self.user}, function (err) {
                        if (err) {
                            logger.info(err.toString());
                            callback && callback(err);
                            return;
                        }
                        callback && callback(null, self.user);
                    });
                } else {
                    logger.info('user not existed');
                    var newUser = {
                        _id: self.openid,
                        openid: self.openid,
                        public_id: self.public_id,
                        create_time: new Date()
                    };
                    if (wxUser) {
                        newUser.is_weixin_info = 1;
                    }
                    utils.mergeJson(newUser, wxUser);
                    coll.insert(newUser, {w: 1}, function (err, doc) {
                        if (err) {
                            logger.info(err.toString());
                            callback && callback(err);
                            return;
                        }
                        self.user = newUser;
                        callback && callback(null, newUser);
                    });
                }
            });
        });
    });
};

MeiweiWeixin.prototype.handleTextMsg = function (callback) {
    var self = this;
    var flag = 0;
    if (this.msg.Content.substring(0, 1) == '#') {
        var l = this.msg.Content.length;
        var keyword = this.msg.Content.substring(1, l);
        if (keyword) {
            if (keyword.length) {
                flag = 1;
            }
        }
    }

    if (flag) {
        var url = CONFIG.API_URL + '/restaurants/restaurant/?keywords=' + keyword;
        this.clickOnRestaurant(url, function (err, result) {
            if (err) {
                logger.info(err.toString());
                callback && callback(null, "抱歉，没有搜索到您要的餐馆");
                return;
            }
            callback && callback(null, result);
        });
    } else {
//        fs.readFile('package.json', function (err, data) {
//            if (err) {
//                console.log(err)
//            }
//            var jsonObj = JSON.parse(data);
//            console.log(jsonObj)
//        });
        callback && callback(new Error('no content'));
//        var content = this.getDefaultTextMsg();
//        if (content) {
//            callback && callback(null, content);
//        } else {
//            callback && callback(new Error('no content'), content);
//        }
    }
};

MeiweiWeixin.prototype.handleLocationMsg = function (callback) {
    var self = this;
    var coordinate = {};
    coordinate.lat = self.msg.Location_X || '31.23524';
    coordinate.lng = self.msg.Location_Y || '121.50582';
    self.coordinateConvert(coordinate, function (coordinate) {
        var url = CONFIG.API_URL + '/restaurants/restaurant/?lat=' + coordinate.lat + '&lng=' + coordinate.lng;
        self.clickOnRestaurant(url, function (err, result) {
            if (err) {
                logger.info(err.toString());
                callback && callback(null, "抱歉，没有找到附近的餐馆");
                return;
            }
            callback && callback(null, result);
        });
    });
};

MeiweiWeixin.prototype.handleSubscribe = function (callback) {
    var self = this;
    this.getWxUserInfo(function (err, result) {
        if (err) {
            var errorMsg = 'get weixin user info error:' + err.toString();
            logger.info(errorMsg);
        }
        var weixinUserInfo = {};
        if (result) {
            utils.mergeJson(weixinUserInfo, result);
            weixinUserInfo.is_weixin_info = 1;
        }
        weixinUserInfo.subscribe = 1;
        weixinUserInfo._id = self.openid;
        weixinUserInfo.openid = self.openid;
        weixinUserInfo.public_id = self.public_id;
        weixinUserInfo.subscribe_time = new Date();
        weixinUserInfo.last_msg_time = new Date();

        self.wxsession.subscribe = 1;

        mongo._get_collection('wx_user', function (err, coll) {
            coll.findOne({_id: weixinUserInfo._id}, function (err, result) {
                if (err) {
                    logger.info(err.toString());
                    callback && callback();
                    return;
                }
                if (!result) {
                    weixinUserInfo.create_time = new Date();
                    coll.insert(weixinUserInfo, {w: 1}, function (err, doc) {
                        if (err) {
                            logger.info(err.toString());
                            callback && callback();
                            return;
                        }
                    });
                } else {
                    delete weixinUserInfo._id;
                    coll.update({
                        _id: self.openid
                    }, {$set: weixinUserInfo}, function (err) {
                        if (err) {
                            logger.info(err.toString());
                            callback && callback();
                            return;
                        }
                        callback && callback();
                    });
                }
            });
        });
    });
};

MeiweiWeixin.prototype.handleUnSubscribe = function (callback) {
    var self = this;
    this.wxsession.destroy();
    mongo._get_collection('wx_user', function (err, coll) {
        if (err) {
            logger.info(err.toString());
            callback && callback();
            return;
        }
        var update = {
            subscribe: 0,
            unsubscribe_time: new Date()
        };
        coll.update({
            _id: self.openid
        }, {$set: update}, function (err) {
            if (err) {
                logger.info(err.toString());
                callback && callback();
                return;
            }
            callback && callback();
            return;
        });
    });
};

// cache scheme , redis ,but not support on windows,mongodb for alternative
MeiweiWeixin.prototype.getCache = function (cacheId, callback) {
    mongo._get_collection('cache', function (err, collection) {
        if (err) {
            callback && callback(err);
        } else {
            collection.findOne({
                _id: cacheId
            }, function (err, cache) {
                try {
                    if (err) {
                        callback && callback(err, null);
                    } else {

                        if (cache) {
                            callback(null, JSON.parse(cache.data));
                        } else {
                            callback && callback('get nothing');
                        }
                    }
                } catch (err) {
                    callback && callback(err);
                }
            });
        }
    });
};

MeiweiWeixin.prototype.saveCache = function (cache, callback) {
    try {
        var s = {
            _id: cache._id,
            data: JSON.stringify(cache.data)
        };

        var today = new Date(), oneDay = 1000 * 60 * 60 * 1 * 1;
        s.expires = new Date(today.getTime() + oneDay);

        mongo._get_collection('cache', function (err, collection) {
            if (err) {
                callback && callback(err);
            } else {
                collection.update({
                    _id: s._id
                }, s, {
                    upsert: true,
                    safe: true
                }, function (err, data) {
                    if (err) {
                        callback && callback(err);
                    } else {
                        callback && callback(null);
                    }
                });
            }
        });
    } catch (err) {
        callback && callback(err);
    }
};


MeiweiWeixin.prototype.getNewsMsg = function (type, callback) {

    var user = this.user || {};

    var PUBLIC_IDS = [CONFIG.SERVICE_PUBLIC_NO];

    var isServiceNo = false;
    var auth2Url = '';
    if (PUBLIC_IDS.indexOf(this.public_id) >= 0) {
        isServiceNo = true;
        auth2Url = 'https://open.weixin.qq.com/connect/oauth2/authorize' +
            '?appid=' + CONFIG.WEIXIN_APPID +
            '&redirect_uri=';
    }

    var isBinding = false;

    if (user.meiwei_token) {
        isBinding = true;
    }

    var bindingMessage = [
        {
            title: '绑定美位账号',
            description: '您还没有绑定美位账号，绑定美位账号，共享优惠服务',
            picUrl: 'http://mmbiz.qpic.cn/mmbiz/icnRhocicxuGM3ZBibZrNmlG1zCHBSyo59fPNvssx6Ijr2YFNShQF7GotjHS0Rmvp9Lus6QHr0wvgibtQdR69omMAg/0',
            url: 'http://www.clubmeiwei.com/weixin/login/?openid=' + this.openid
        }
    ];

    if (isBinding) {
        bindingMessage[0].url = 'http://www.clubmeiwei.com/weixin/login/?openid=' + this.openid + '&meiwei_token=' + user.meiwei_token;
    }

    var news = null;
    switch (type) {
        case 'V100_NEW_PARTY':
            news = [
                {
                    title: '美位聚会',
                    description: '还在为聚会决策犹豫不决吗，通过美位发起聚会，让你的朋友投票，决定聚会时间，地点，不再犹豫！',
                    picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadimages.jpg'
                }
            ];
            news[0].url = auth2Url + encodeURIComponent('http://mobile.clubmeiwei.com/together/myparty') +
                '&response_type=code&scope=snsapi_base&state=party#wechat_redirect';
            callback && callback(null, news);
            break;
        case 'V100_NEW_RESTAURANT':
            news = [
                wechatPost.NEW_RESTAURANT_0,
                wechatPost.NEW_RESTAURANT_1,
                wechatPost.NEW_RESTAURANT_2,
                wechatPost.NEW_RESTAURANT_3
            ];
            callback && callback(null, news);
            break;
        case 'V100_NEW_PRODUCT':
            news = [
                wechatPost.NEW_PRODUCT_0,
                wechatPost.NEW_PRODUCT_1,
                wechatPost.NEW_PRODUCT_2,
                wechatPost.NEW_PRODUCT_3,
                wechatPost.NEW_PRODUCT_4
            ];
            callback && callback(null, news);
            break;
        case 'V100_NEW_EVENT':
            news = [
                wechatPost.NEW_EVENT_0,
                wechatPost.NEW_EVENT_1
            ];
            callback && callback(null, news);
            break;


        case 'V100_RESTAURANT_SERVICE':
                news = [
                    {
                        title: '餐饮预订',
                        description: '百家餐厅，任您选择',
                    picUrl: 'http://meiwei.u.qiniudn.com/media/restaurant/2a125daf7022437c4b6bee7587c28fec_1.jpg'
                    },
                    {
                        title: '精选红酒',
                        description: '享有第一个超高档威士忌盛誉的皇家礼炮，以其细腻的口感与独特的韵味，受到全世界的推崇与赞赏。',
                    picUrl: 'http://meiwei.u.qiniudn.com/custom/upload1649070634.jpg'
                    },
                    {
                        title: '代驾服务',
                        description: '代驾服务',
                    picUrl: 'http://meiwei.u.qiniudn.com/custom/upload286222160.jpg'
                    },
                    {
                        title: '挡酒师服务',
                        description: '忙于商务应酬的您，觥筹交错之间，难免不胜酒力！欢迎选用美位独家挡酒师服务，男女可选，酒量保证',
                    picUrl: 'http://meiwei.u.qiniudn.com/media/product/bbcc228015dbc9788252118bbdae6f1d2ddc0955.jpg'
                    }
                ];
            if (isServiceNo) {
                if (isBinding) {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/restaurant/search') +
                        '&response_type=code&scope=snsapi_base&state=restaurant_search#wechat_redirect';//#restaurant/search';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/p31') +
                        '&response_type=code&scope=snsapi_base&state=jingxuanhongjiu#wechat_redirect';//'/#product/p31';
                    news[2].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/requestdriver') +
                        '&response_type=code&scope=snsapi_base&state=daijia#wechat_redirect';//'/#requestdriver';
                    news[3].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/90/order') +
                        '&response_type=code&scope=snsapi_base&state=dangjiushi#wechat_redirect';//'/#product/90/order';
                } else {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/restaurant/search') +
                        '&response_type=code&scope=snsapi_base&state=restaurant_search#wechat_redirect';//#restaurant/search';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/p31') +
                        '&response_type=code&scope=snsapi_base&state=jingxuanhongjiu#wechat_redirect';//'/#product/p31';
                    news[2].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/requestdriver') +
                        '&response_type=code&scope=snsapi_base&state=daijia#wechat_redirect';//'/#requestdriver';
                    news[3].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/90/order') +
                        '&response_type=code&scope=snsapi_base&state=dangjiushi#wechat_redirect';//'/#product/90/order';
                }
            } else {
                if (isBinding) {
                    news[0].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/restaurant/search';
                    news[1].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/product/p31';
                    news[2].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/requestdriver';
                    news[3].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/product/90/order';
                } else {
                    news[0].url = CONFIG.MOBILE_SITE_URL + '#restaurant/search';
                    news[1].url = CONFIG.MOBILE_SITE_URL + '#product/p31';
                    news[2].url = CONFIG.MOBILE_SITE_URL + '#requestdriver';
                    news[3].url = CONFIG.MOBILE_SITE_URL + '#product/90/order';
                    }

            }
            callback && callback(null, news);
            break;
        case 'V100_FESTIVAL_SERVICE':
                news = [
                    {
                        title: '鲜花定制',
                        description: '我们将根据您的送花对象、送花场合，以及所有其他可能影响到鲜花的因素，为您私家定制一份独一无二的礼物',
                    picUrl: 'http://meiwei.u.qiniudn.com/media/product/e9ce78bdfb196ca3e590da07aba64144659b02cb.jpg'
                    },
                    {
                        title: '蛋糕定制',
                        description: '用您的灵巧心思，和我们一起携手，给宝宝一个生日惊喜。',
                    picUrl: 'http://meiwei.u.qiniudn.com/media/product/6805dfbf6cecd466beed3b4d8e7a195098f1003f.jpg'
                    },
                    wechatPost.JIERIFANGANCEHUA
                ];
            if (isServiceNo) {
                if (isBinding) {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/p21') +
                        '&response_type=code&scope=snsapi_base&state=xianhuadingzhi#wechat_redirect';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/p39') +
                        '&response_type=code&scope=snsapi_base&state=dangaodingzhi#wechat_redirect';
                } else {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/p21') +
                        '&response_type=code&scope=snsapi_base&state=xianhuadingzhi#wechat_redirect';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/p39') +
                        '&response_type=code&scope=snsapi_base&state=dangaodingzhi#wechat_redirect';
                }
            } else {
                if (isBinding) {
                    news[0].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/product/p21';
                    news[1].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/product/p39';
            } else {
                    news[0].url = CONFIG.MOBILE_SITE_URL + '#product/p21';
                    news[1].url = CONFIG.MOBILE_SITE_URL + '#product/p39';
                }
            }
            callback && callback(null, news);
            break;
        case 'V100_EVENT_SERVICE':
                news = [
                    wechatPost.CEHUAZHIXING,
                    wechatPost.CHANGDITUIJIAN,
                    {
                        title: '美妆服务',
                        description: '帮漂亮的你还想进一步容颜升级？美位美妆师，帮你量身打造甜美妆容，轻松俘获他的心',
                    picUrl: 'http://meiwei.u.qiniudn.com/media/product/73c3953f1875931386af18f6adfc2fc2deb1219e.jpg'
                    }
                ];
            if (isServiceNo) {
                if (isBinding) {
                    news[2].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/p32') +
                        '&response_type=code&scope=snsapi_base&state=meizhuangfuwu#wechat_redirect';
                } else {
                    news[2].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/p32') +
                        '&response_type=code&scope=snsapi_base&state=meizhuangfuwu#wechat_redirect';
                }
            } else {
                if (isBinding) {
                    news[2].url = CONFIG.MOBILE_SITE_URL +
                        '#regAuth/' + user.meiwei_token + '/product/p32';
            } else {
                    news[2].url = CONFIG.MOBILE_SITE_URL + '#product/p32';
                    }
            }
            callback && callback(null, news);
            break;
        case 'V100_TRIP_SERVICE':
                news = [
                    {
                        title: 'VVIP服务',
                        description: 'VVIP接送机服务旨在为会员提供最高端的个人及商务服务和最全面的休闲设施。',
                    picUrl: 'http://meiwei.u.qiniudn.com/custom/upload428383236.jpg'
                    },
                    {
                        title: '商务出行',
                        description: '结婚、度假、接送贵宾……车自然是越豪华越好~还可以为您配备专业司机哦~',
                    picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadvvip-small.jpg'
                    },
                    wechatPost.GAODUANSHANGLV
                ];
            if (isServiceNo) {
                if (isBinding) {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/vvip') +
                        '&response_type=code&scope=snsapi_base&state=vvip#wechat_redirect';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token + '/product/p29') +
                        '&response_type=code&scope=snsapi_base&state=shangwuchuxing#wechat_redirect';
                } else {
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/vvip') +
                        '&response_type=code&scope=snsapi_base&state=vvip#wechat_redirect';
                    news[1].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/0/product/p29') +
                        '&response_type=code&scope=snsapi_base&state=shangwuchuxing#wechat_redirect';
                }
            } else {
                if (isBinding) {
                    news[0].url = CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/vvip';
                    news[1].url = CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/product/p29';
            } else {
                    news[0].url = CONFIG.MOBILE_SITE_URL + '#vvip';
                    news[1].url = CONFIG.MOBILE_SITE_URL + '#product/p29';
                }
            }
            callback && callback(null, news);
            break;
        case 'V100_BUSINESS_SERVICE':
            news = [
                wechatPost.CANYINGFUWU,
                wechatPost.HUIWUZHIXING,
                wechatPost.QIYEDIAOYAN
            ];
            callback && callback(null, news);
            break;


        case 'V100_MY_ACCOUNT':
            if (isBinding) {
                news = [
                    {
                        title: '绑定美位账号',
                        description: '您已经绑定美位账号，重新绑定请点击图文消息',
                        picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                        url: 'http://www.clubmeiwei.com/weixin/login/?openid=' + this.openid + '&meiwei_token=' + (user.meiwei_token || '')
                    }
                ];
            } else {
                news = [
                    {
                        title: '绑定美位账号',
                        description: '绑定美位账号，共享优惠服务',
                        picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                        url: 'http://www.clubmeiwei.com/weixin/login/?openid=' + this.openid
                    }
                ];
            }

            callback && callback(null, news);
            break;
        case 'V100_MY_RESTAURANT':
            if (isServiceNo) {
                if (isBinding) {
                    news = [
                        {
                            title: '点击查看我的餐厅预订',
                            description: '',
                            picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png'
                        }
                    ];
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token +'/order') +
                        '&response_type=code&scope=snsapi_base&state=vvip#wechat_redirect';
                } else {
                    news = bindingMessage;
                }
            } else {
            if (isBinding) {
                news = [
                    {
                        title: '点击查看我的餐厅预订',
                        description: '',
                        picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                        url: CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/order'
                    }
                ];
            } else {
                news = bindingMessage;
            }
            }
            callback && callback(null, news);
            break;
        case 'V100_MY_ORDER':
            if(isServiceNo) {
                if (isBinding) {
                    news = [
                        {
                            title: '我的订单',
                            description: '查看我的订单',
                            picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png'
                        }
                    ];
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token +'/genericorder') +
                        '&response_type=code&scope=snsapi_base&state=vvip#wechat_redirect';
                } else {
                    news = bindingMessage;
                }
            } else {
            if (isBinding) {
                news = [
                    {
                        title: '我的订单',
                        description: '查看我的订单',
                        picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                        url: CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/genericorder'
                    }
                ];
            } else {
                news = bindingMessage;
            }
            }
            callback && callback(null, news);
            break;
        case 'V100_MY_COUPON':
            if(isServiceNo) {
                if (isBinding) {
                    news = [
                        {
                            title: '我的优惠券',
                            description: '我的优惠券',
                            picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                            url: CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/member'
                        }
                    ];
                    news[0].url = auth2Url + encodeURIComponent(CONFIG.WEIXIN_AUTH2_REDIRECT_URL +
                        '#wxAuth/' + user.meiwei_token +'/member') +
                        '&response_type=code&scope=snsapi_base&state=vvip#wechat_redirect';
                } else {
                    news = bindingMessage;
                }
            } else {
            if (isBinding) {
                news = [
                    {
                        title: '我的优惠券',
                        description: '我的优惠券',
                        picUrl: 'http://meiwei.u.qiniudn.com/custom/uploadbg-top@2x.png',
                        url: CONFIG.MOBILE_SITE_URL + '#regAuth/' + user.meiwei_token + '/member'
                    }
                ];
            } else {
                news = bindingMessage;
            }
            }
            callback && callback(null, news);
            break;
        default :
            callback && callback(new Error('no key event'));
            break;
    }
};

MeiweiWeixin.prototype.handleDefault = function (callback) {
        var err = new Error('no need response');
        callback && callback(err);
//    var content = this.getDefaultTextMsg();
//    if (content) {
//        callback && callback(null, content);
//        return;
//    } else {
//        var err = new Error('no need response');
//        callback && callback(err);
//    }
};

MeiweiWeixin.prototype.handleEvent = function (callback) {
    var self = this;
    var event = self.msg.Event;
    var eventKey = self.msg.EventKey;
    if (event == 'CLICK') {
        switch (eventKey) {
            case 'V100_RESTAURANT_LUCKY':
                var coordinate = {};
                if (self.wxsession) {
                    coordinate.lat = self.wxsession.latitude || '31.23524';
                    coordinate.lng = self.wxsession.longitude || '121.50582';
                } else {
                    coordinate.lat = '31.23524';
                    coordinate.lng = '121.50582';
                }
                self.coordinateConvert(coordinate, function (coordinate) {
                    var url = CONFIG.API_URL + '/restaurants/restaurant/?lat=' + coordinate.lat + '&lng=' + coordinate.lng;
                    self.clickOnRestaurant(url, function (err, result) {
                        callback && callback(err, result);
                    });
                });
                break;
            case 'V100_RESTAURANT_AROUND':
                var coordinate = {};
                if (self.wxsession) {
                    coordinate.lat = self.wxsession.latitude || '31.23524';
                    coordinate.lng = self.wxsession.longitude || '121.50582';
                } else {
                    coordinate.lat = '31.23524';
                    coordinate.lng = '121.50582';
                }
                self.coordinateConvert(coordinate, function (coordinate) {
                    var url = CONFIG.API_URL + '/restaurants/restaurant/?lat=' + coordinate.lat + '&lng=' + coordinate.lng;
                    self.clickOnRestaurant(url, function (err, result) {
                        callback && callback(err, result);
                    });
                });
                break;
            case 'V100_RESTAURANT_SERVICE':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_FESTIVAL_SERVICE':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_EVENT_SERVICE':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_TRIP_SERVICE':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_BUSINESS_SERVICE':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_NEW_PRODUCT':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_NEW_PARTY':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_NEW_RESTAURANT':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_NEW_EVENT':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_MY_ACCOUNT':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_MY_ORDER':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_MY_RESTAURANT':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            case 'V100_MY_COUPON':
                self.getNewsMsg(eventKey, function (err, result) {
                    callback && callback(err, result);
                });
                break;
            default:
                callback && callback(null);
                break;
        }
    } else if (event == 'subscribe') {
        self.handleSubscribe(function () {
            var content = self.getDefaultTextMsg();
            if (content) {
                callback(null, content);
            } else {
                callback(new Error('no need to send'));
            }
            return;
        });
    } else if (event == 'unsubscribe') {
        self.handleUnSubscribe(function () {
            callback && callback();
            return;
        });
    } else if (event == 'scan') {
        callback(new Error('no need to send'));
        return;
        // already subscribe , scan qr again
    } else if (event == 'LOCATION') {
        if (!self.wxsession) {
            self.wxsession = {};
        }
        self.wxsession.latitude = self.msg.Latitude;
        self.wxsession.longitude = self.msg.Longitude;
        self.wxsession.precision = self.msg.Precision;
        callback(null)
    }

};

MeiweiWeixin.prototype.handleWeixinRequest = function () {
    var self = this;
    //get user information
    this.getOrSaveUser(function (err, user) {
        if (err) {
            logger.info(err.toString());
        }
        if (user) {
            self.user = user;
        }
        switch (self.msg.MsgType) {
            case 'text' :
                self.handleTextMsg(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        logger.info(err.toString());
                        if(self.public_id==CONFIG.SERVICE_PUBLIC_NO) {
                            var createTime = new Date().getTime();
                            self.res.send('<xml>'+
                                '<ToUserName><![CDATA[' + self.openid + ']]></ToUserName>'+
                                '<FromUserName><![CDATA[' + self.public_id + ']]></FromUserName>'+
                                '<CreateTime>' + createTime + '</CreateTime>'+
                                '<MsgType><![CDATA[transfer_customer_service]]></MsgType>'+
                                '</xml>');
                            return;
                        } else {
                            if(CONFIG.SERVICE_TRANSFER) {
				logger.info("send sms")
                                self.wxServiceMessage(self.msg.Content);
                                self.res.reply(result);
                                return;
                            } else {
                                self.res.reply(result);
                                return;
                            }
                        }
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                });
                break;
            case 'image' :
                self.handleDefault(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        self.res.end();
                        logger.info(err.toString());
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            case 'voice' :
                self.handleDefault(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        self.res.end();
                        logger.info(err.toString());
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            case 'video' :
                self.handleDefault(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        self.res.end();
                        logger.info(err.toString());
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            case 'location' :
                self.handleLocationMsg(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        logger.info(err.toString());
                        self.res.end();
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            case 'link' :
                self.handleDefault(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        self.res.end();
                        logger.info(err.toString());
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            case 'event' :
                self.handleEvent(function (err, result) {
                    var event = self.msg.Event;
                    if (event != 'LOCATION') {
                        self.wxsession.last_msg_time = new Date();
                    }
                    if (err) {
                        logger.info(err.toString());
                        self.res.end();
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
            default :
                self.handleDefault(function (err, result) {
                    self.wxsession.last_msg_time = new Date();
                    if (err) {
                        self.res.end();
                        logger.info(err.toString());
                        return;
                    }
                    self.res.reply(result);
                    logger.info(JSON.stringify(result));
                    return;
                });
                break;
        }
    });
};

MeiweiWeixin.prototype.wxServiceMessage = function (content) {
    this.getWxAccessToken(function (err, accessToken) {
        if (err) {
            logger.info(err.toString());
            return;
        }

        var postUrl = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' + accessToken;
        var data = {
            'touser': CONFIG.SERVICE_NO,
            "msgtype": "text",
            "text": {
                "content": content
            }
        };

        data = JSON.stringify(data);
        var opt = {
            method: "POST",
            hostname: "api.weixin.qq.com",
            path: '/cgi-bin/message/custom/send?access_token=' + accessToken,
            headers: {
                "Content-Type": 'application/json'
            }
        };
        var req = https.request(opt, function (serverFeedback) {
            if (serverFeedback.statusCode == 200) {
                var body = "";
                serverFeedback.on('data',function (data) {
                    body += data;
                }).on('end', function () {
                        logger.info(body);
                    });
            }
            else {
                logger.info("error");
            }
        });
        req.write(data + "\n");
        req.end();
    });
};


module.exports = MeiweiWeixin;


