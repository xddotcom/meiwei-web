var events = require('events');
var emitter = new events.EventEmitter();
var http = require('http');
var https = require('https');
var mongo = require('../utils/mongo-connect');
var uuid = require('node-uuid');
var fs = require('fs');
var crypto = require('crypto');

var CONFIG = require('../settings/settings');
var logger = require('../utils/logger.js');
var utils = require('../utils/utils.js');

var MeiweiWeixin = require('./meiwei-weixin.js');

function getWxAccessToken(callback) {
    var self = this;
    var now = Math.round(new Date().getTime() / 1000);
    mongo._get_collection('weixin_access_token', function (err, collection) {
        if (err) {
            callback && callback(err);
            return;
        }
        collection.findOne({_id: 'gh_acaf6fef0fbf'}, function (err, weixinAccessToken) {
            if (err) {
                callback && callback(err);
                return;
            }
            if (weixinAccessToken) {
                if (((now - weixinAccessToken.create_time) - 3600) < 0) {
                    callback && callback(null, weixinAccessToken.access_token);
                    return;
                }
            }
            var accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' +
                CONFIG.WEIXIN_APPID + '&secret=' + CONFIG.WEIXIN_APPSECRET;
            var _data = '';
            var accessToken = '';
            https.get(accessTokenUrl,function (response) {
                if (response.statusCode >= 300) {
                    var errorMsg = "get access_token from weixin server return not 200";
                    var err = new Error(errorMsg);
                    callback(err);
                    return;
                }

                response.on('data',function (chunk) {
                    _data += chunk;
                }).on('end', function () {
                        try {
                            var json = JSON.parse(_data);
                            if (json.errcode) {
                                var errorMsg = 'get access_token error:' + json.errmsg;
                                var err = new Error(errorMsg);
                                callback(err);
                                return;
                            } else {
                                accessToken = json.access_token;
                                var weixinAccessToken = {
                                    _id: 'gh_acaf6fef0fbf',
                                    access_token: json.access_token,
                                    create_time: Math.round(new Date().getTime() / 1000)
                                };

                                collection.update({
                                    _id: weixinAccessToken._id
                                }, weixinAccessToken, {
                                    upsert: true,
                                    safe: true
                                }, function (err, data) {
                                    if (err) {
                                        callback && callback(err);
                                        return;
                                    }
                                    callback(null, accessToken);
                                    return;
                                });
                            }
                        } catch (e) {
                            var errorMsg = 'json parse error' + e.toString();
                            var err = new Error(errorMsg);
                            callback(err);
                            return;
                        }
                    });
            }).on('error', function (e) {
                    var errorMsg = "delete access_token from weixin server network error:" + e.message;
                    var err = new Error(errorMsg);
                    callback(err);
                    return;
                });

        });

    });
};


// 服务号，view 菜单点击认证后回调。
exports.wxCallbackAuth = function (req, res) {
    var code = req.query.code;
    var state = req.query.state;

    if (!code) {
        res.redirect(CONFIG.MOBILE_SITE_URL);
        return 0;
    }

    var stateMapping = {
        'restaurant_search': 'restaurant/search',
        'jingxuanhongjiu': 'product/p31',
        'daijia': 'requestdriver',
        'dangjiushi': 'product/90/order',
        'xianhuadingzhi': 'product/p21',
        'dangaodingzhi': 'product/p39',
        'meizhuangfuwu': 'product/p32',
        'vvip': 'vvip',
        'shangwuchuxing': 'product/p29',
        'my_restaurant': 'order',
        'my_genericorder': 'genericorder',
        'my_coupon': 'member',
        'weixin_login': 'http://www.clubmeiwei.com/weixin/login/'
    };

    var goToPage = function (state, accesstoken, user) {
        var url = '';
        var isBinding = false;
        if (user.meiwei_token) {
            isBinding = true;
        }

        if (state == stateMapping.weixin_login) {
            url += 'http://www.clubmeiwei.com/weixin/login/?openid=' + this.openid + '&accesstoken=' + accesstoken + '&meiwei_token=' + (user.meiwei_token || '')
        } else {
            if (isBinding) {
                url = CONFIG.MOBILE_SITE_URL + '&openid=' + user.openid + '&code=' + code + '&state=' + state +
                    '/#wxAuth/' + accesstoken + '/' + user.meiwei_token + '/' + stateMapping[state]
            } else {
                url = CONFIG.MOBILE_SITE_URL + '&openid=' + user.openid + '&code=' + code + '&state=' + state +
                    '/#wxAuth/' + accesstoken + '/0/' + stateMapping[state]
            }
        }
        res.redirect(url);
        return 0;
    };
    var getAuthAccessToken = function (accessTokenUrl, callback) {
        https.get(accessTokenUrl,function (response) {
            if (response.statusCode >= 300) {
                var errorMsg = "get access_token from weixin server return not 200";
                var err = new Error(errorMsg);
                callback(err);
                return;
            }
            var _data = '';
            response.on('data',function (chunk) {
                _data += chunk;
            }).on('end', function () {
                    try {
                        var json = JSON.parse(_data);
                        if (json.errcode) {
                            var errorMsg = 'get access_token error:' + json.errmsg;
                            var err = new Error(errorMsg);
                            callback(err);
                            return;
                        } else {
                            callback(null, json);
                        }
                    } catch (err) {
                        callback && callback(err);
                        return;
                    }
                });
        }).on('error', function () {
                var errorMsg = "get access_token from weixin server network error:" + e.message;
                var err = new Error(errorMsg);
                callback(err);
                return;
            });
    };

    var accessTokenUrl = 'https://api.weixin.qq.com/sns/oauth2/access_token?' +
        'appid=' + CONFIG.WEIXIN_APPID +
        '&secret=' + CONFIG.WEIXIN_APPSECRET +
        '&code=' + code +
        '&grant_type=authorization_code';

    getAuthAccessToken(accessTokenUrl, function (err, result) {
        if (err) {
            logger.info(err.toString());
            res.redirect(CONFIG.MOBILE_SITE_URL);
            return 0;
        }

        var openid = result.openid;
        mongo._get_collection('wx_user', function (err, coll) {
            if (err) {
                logger.info(err.toString());
                res.redirect(CONFIG.MOBILE_SITE_URL);
                return;
            }
            coll.findOne({_id: openid}, function (err, user) {
                if (err) {
                    logger.info(err.toString());
                    res.redirect(CONFIG.MOBILE_SITE_URL);
                    return;
                }
                result.get_access_token_time = new Date();
                if (!user) {
                    result._id = openid;
                    result.create_time = result.get_access_token_time;
                    coll.insert(result);
                    res.redirect(CONFIG.MOBILE_SITE_URL);
                    return;
                } else {
                    coll.update({
                        _id: openid
                    }, {$set: result}, function (err) {
                        if (err) {
                            logger.info(err.toString());
                        }
                        goToPage(state, result.access_token, user);
                    });
                }
            });
        });

    });
};

exports.menu = function (req, res) {
    var wxAuthUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?';
    wxAuthUrl += 'appid=' + CONFIG.WEIXIN_APPID;
    var parameter = '&response_type=code&scope=snsapi_base&state=1#wechat_redirect';
    var redirectUri = CONFIG.WEIXIN_AUTH_REDIRECT_URI;

    var auth2Url = 'https://open.weixin.qq.com/connect/oauth2/authorize' +
        '?appid=' + CONFIG.WEIXIN_APPID +
        '&redirect_uri=';

    var partyUrl = auth2Url + encodeURIComponent('http://mobile.clubmeiwei.com/together/') +
        '&response_type=code&scope=snsapi_base&state=party#wechat_redirect';

    var plainUrl = 'http://mobile.clubmeiwei.com/weixin/weixin.html?showwxpaytitle=1#product/175/order';

    var menu = {
        "button": [
            {
                "name": "新登场",
                "sub_button": [
                    {
                        "type": "view",
                        "name": "美位夜店",
                        "url": "http://mobile.clubmeiwei.com/weixin/weixin.html?showwxpaytitle=1#home/l31"
                    },
                    {
                        "type": "click",
                        "name": "新活动",
                        "key": "V100_NEW_EVENT"
                    },
                    {
                        "type": "click",
                        "name": "新餐厅",
                        "key": "V100_NEW_RESTAURANT"
                    },
//                    {
//                        "type": "view",
//                        "name": "美位聚会",
//                        "url": partyUrl
//                    },
                    {
                        "type": "view",
                        "name": "美位·飞体验",
                        "url": plainUrl
                    },
                    {
                        "type": "view",
                        "name": "微信小店",
                        "url": 'http://mp.weixin.qq.com/bizmall/mallshelf?id=&t=mall/list&biz=MzA5MjA2ODIwOQ==&shelf_id=5&showwxpaytitle=1#wechat_redirect'
                    }

                ]
            },
            {
                "name": "美位管家",
                "sub_button": [
                    {
                        "type": "click",
                        "name": "餐饮管家",
                        "key": "V100_RESTAURANT_SERVICE"
                    },
                    {
                        "type": "click",
                        "name": "节日管家",
                        "key": "V100_FESTIVAL_SERVICE"
                    },
                    {
                        "type": "click",
                        "name": "活动管家",
                        "key": "V100_EVENT_SERVICE"
                    },
                    {
                        "type": "click",
                        "name": "出行管家",
                        "key": "V100_TRIP_SERVICE"
                    },
                    {
                        "type": "click",
                        "name": "企业管家",
                        "key": "V100_BUSINESS_SERVICE"
                    }
                ]
            },

            {
                "name": "个人账户",
                "sub_button": [
                    {
                        "type": "click",
                        "name": "绑定账号",
                        "key": "V100_MY_ACCOUNT"
                    },
                    {
                        "type": "click",
                        "name": "我的订单",
                        "key": "V100_MY_ORDER"
                    },
                    {
                        "type": "click",
                        "name": "我的餐厅预订",
                        "key": "V100_MY_RESTAURANT"
                    },
                    {
                        "type": "view",
                        "name": "我的优惠券",
                        "url": "http://mobile.clubmeiwei.com/weixin/vip?showwxpaytitle=1"
                    },
                    {
                        "type": "click",
                        "name": "维权",
                        "key": "V100_MY_RIGHT"
                    }
                ]
            }
        ]
    }

    res.json(menu);
};

exports.wxlogin = function (req, res) {

    var openid = req.query.openid;

    var errorMsg = '';

    if (!openid) {
        errorMsg = '没有微信openid';
        logger.info(4000, req.originalUrl, req.ip, errorMsg);
        //res.redirect('/');
        //return;
    }

    var weixinInfo = {};

    weixinInfo.auth_deny = 1;
    weixinInfo.is_weixin = req.query.is_weixin || req.body.is_weixin || 0;
    weixinInfo.openid = openid;

    res.render('member/wxlogin.html', {
        SERVER_DATA: JSON.stringify({
            status: 'signin'
        }),
        STATIC_URL: CONFIG.STATIC_URL,
        STATIC_JS: ['views/wx-login.js'],
        STATIC_CSS: ['views/wx-login.css'],
        BUILD: CONFIG.BUILD,
        weixin_info: JSON.stringify(weixinInfo)
    });

};

exports.wxBinding = function (req, res) {

    var token = req.body.token;
    var openid = req.body.openid;

    if (!openid) {
        var errorMsg = 'openid is null';
        logger.info(4000, req.originalUrl, req.ip, errorMsg);
        res.send(400, errorMsg);
        return;
    }
    if (!token) {
        var errorMsg = 'token is null';
        logger.info(4000, req.originalUrl, req.ip, errorMsg);
        res.send(400, errorMsg);
        return;
    }

    var now = new Date();
    var set = {
        bingding_time: now,
        meiwei_token: token
    };

    var options = {
        hostname: CONFIG.API_HOSTNAME,
        port: CONFIG.API_PORT,
        path: '/members/profile/',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;encoding=utf-8',
            'Authorization': 'token ' + token
        }
    };

    http.get(options,function (response) {
        if (response.statusCode >= 300) {
            var errorMsg = "return not 200";
            logger.info(4000, req.originalUrl, req.ip, errorMsg);
            mongo._get_collection('wx_user', function (err, coll) {
                if (err) {
                    var errorMsg = 'mongodb get collection error:' + err.toString();
                    logger.info(4000, req.originalUrl, req.ip, errorMsg);
                    res.send(400, errorMsg);
                    return;
                }
                coll.update({
                    _id: openid
                }, {
                    $set: set
                }, {
                    w: 1
                }, function (err, result) {
                    if (err) {
                        var errorMsg = "update meiwei_user error:" + err.toString();
                        logger.info(4000, req.originalUrl, req.ip, errorMsg);
                        res.send(400, errorMsg);
                        return;
                    }
                    res.send({});
                    return;
                });
            });
            return;
        } else {
            response.setEncoding('utf-8');
            var responseString = '';
            response.on('data', function (data) {
                responseString += data;
            });

            response.on('end', function () {
                var resultObject;
                var flag = 1;
                try {
                    resultObject = JSON.parse(responseString);
                } catch (e) {
                    var errorMsg = "JSON.parse meiwei_user error:" + e.toString();
                    logger.info(4000, req.originalUrl, req.ip, errorMsg);
                    flag = 0;
                }

                if (flag) {
                    var meiweiUserInfo = resultObject.results[0];
                    for (var key in meiweiUserInfo) {
                        set['meiwei_' + key] = meiweiUserInfo[key];
                    }
                }

                mongo._get_collection('wx_user', function (err, coll) {
                    if (err) {
                        var errorMsg = 'mongodb get collection error:' + err.toString();
                        logger.info(4000, req.originalUrl, req.ip, errorMsg);
                        res.send(400, errorMsg);
                        return;
                    }
                    coll.update({
                        _id: openid
                    }, {
                        $set: set
                    }, {
                        w: 1
                    }, function (err, result) {
                        if (err) {
                            var errorMsg = "update wx_user error:" + err.toString();
                            logger.info(4000, req.originalUrl, req.ip, errorMsg);
                            res.send(400, errorMsg);
                            return;
                        }
                        res.send({});
                        return;
                    });
                });
            });
        }
    }).on('error', function (e) {
            var errorMsg = "connect to meiwei server error" + e.toString();
            logger.info(4000, req.originalUrl, req.ip, errorMsg);
            mongo._get_collection('wx_user', function (err, coll) {
                if (err) {
                    var errorMsg = 'mongodb get collection error:' + err.toString();
                    logger.info(4000, req.originalUrl, req.ip, errorMsg);
                    res.send(400, errorMsg);
                    return;
                }
                coll.update({
                    _id: openid
                }, {
                    $set: set
                }, {
                    w: 1
                }, function (err, result) {
                    if (err) {
                        var errorMsg = "update wx_user error:" + err.toString();
                        logger.info(4000, req.originalUrl, req.ip, errorMsg);
                        res.send(400, errorMsg);
                        return;
                    }
                    res.send({});
                    return;
                });
            });
        });
};

exports.handleWxRequest = function (req, res, next) {
    var meiweiWeixin = new MeiweiWeixin(req, res, next);
    meiweiWeixin.handleWeixinRequest();
};


exports.wxServiceMessage = function (req, res) {
    var openid = req.body.openid;
    var content = req.body.content;

    getWxAccessToken(function (err, accessToken) {
        if (err) {
            logger.info(err.toString());
            res.send(err.toString());
            return;
        }

        var postUrl = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=' + accessToken;
        var data = {
            'touser': openid,
            "msgtype": "text",
            "text": {
                "content": content
            }
        };

        data = JSON.stringify(data);
        var opt = {
            method: "POST",
            hostname: "api.weixin.qq.com",
            path: '/cgi-bin/message/custom/send?access_token=' + accessToken,
            headers: {
                "Content-Type": 'application/json'
            }
        };
        var req = https.request(opt, function (serverFeedback) {
            if (serverFeedback.statusCode == 200) {
                var body = "";
                serverFeedback.on('data',function (data) {
                    body += data;
                }).on('end', function () {
                        logger.info(body);
                        res.send(200, body);
                    });
            }
            else {
                logger.info("error");
                res.send(500, "error");
            }
        });
        req.write(data + "\n");
        req.end();
    });
};
