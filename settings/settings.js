/**
 * Created by weixumian on 13-10-31.
 */

var CONFIG = {
    BINDING_IP: "127.0.0.1",
    LISTEN_PORT: 8081,
    STATIC_URL: '/assets/',

    API_URL: 'http://api.clubmeiwei.com',
    API_HOSTNAME: 'api.clubmeiwei.com',
    API_PORT: 80,
    SITE_URL: "http://www.clubmeiwei.com",
    MOBILE_SITE_URL: 'http://mobile.clubmeiwei.com/weixin/weixin.html?showwxpaytitle=1',

    PROXY_ON: 1,

    WEIXIN_TOKEN: '1qaz2wsx3edc',
    WEIXIN_APPID: 'wx0fd053b2f2f80d94',
    WEIXIN_APPSECRET: '060a1300bb8e8479afaae12b1aa0ad85',
    WEIXIN_AUTH_REDIRECT_URI: 'www.clubmeiwei.com',

    WEIXIN_AUTH_SERVER_URL: 'https://open.weixin.qq.com/connect/oauth2/authorize',
    WEIXIN_AUTH2_REDIRECT_URL: 'http://mobile.clubmeiwei.com/weixin/weixin.html?showwxpaytitle=1',

    MONGO_SERVER: "115.29.52.187",
    MONGO_PORT: 27017,
    MONGO_USER: "meiwei",
    MONGO_PASSWORD: "meiwei",
    MONGO_DB: "meiwei",

    SERVICE_PUBLIC_NO: 'gh_acaf6fef0fbf',
    SERVICE_TRANSFER: 1,
//    SERVICE_NO: 'ocTdNuHCAY2-p9m6r-ISeI6SAJog', //XD
//    SERVICE_NO: 'ocTdNuLQGZFv6XN2jKYtI7CrUhKo', //David
//    SERVICE_NO: 'ocTdNuDwU5lvZJXJrsD36GKlvMJo', //Even
//    SERVICE_NO: 'ocTdNuOJUX49x711pKJC8fyvrmy4',  //Emilly
//    SERVICE_NO: 'ocTdNuIVbGSCHw3dbrA-BZrmFw3s',  //ROy
//    SERVICE_NO: 'ocTdNuCxl4T-ocCFjyL2mfzm9Rag',   // bobby
    SERVICE_NO: 'ocTdNuOjndiC3KdtndjwvVa7o9q8', // Maureen
//    SERVICE_NO: 'ocTdNuK-POYyg-4_xt06IH43I6eI', //lili


    LOGGER_LEVEL: 'INFO',
    KEY: '1qaz2wsx3edc4rfv',
    BUILD: '0318'
};

try {
    var CONFIG_LOCAL = require('./settings_local');
    var utils = require('../utils/utils');
    utils.mergeJson(CONFIG, CONFIG_LOCAL);
} catch (e) {
    //console.log(e);
}

module.exports = CONFIG;
